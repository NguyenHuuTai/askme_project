<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $num_per_page = 6 * $page;
    $start = 0;
    $list_post = get_list_post($start, $num_per_page);
    $data['list_post'] = $list_post;
    $data['page'] = $page;
    $data['start'] = $start;
    #lấy danh sách tag và categories
    $list_categories = get_list_categories();
    $list_tag = get_list_tag();
    //show_array($list_categories);
    $data['list_categories'] = $list_categories;
    $data['list_tag'] = $list_tag;
    //load trên view
    load_view('index', $data);
}

function searchAction() {
    global $tittle;
    if (isset($_POST['btn-search'])) {
        if (empty($_POST['title'])) {
            redirect('?mod=home&controller=index');
        } else {
            $title = $_POST['title'];
        }
        $list_post = search_post_by_title($title);
        $data['list_post'] = $list_post;
        #lấy danh sách tag và categories
        $list_categories = get_list_categories();
        $list_tag = get_list_tag();
        //show_array($list_post);
        $data['list_categories'] = $list_categories;
        $data['list_tag'] = $list_tag;
    }
    load_view('search', $data);
}

function categoryAction() {
    $id=$_GET['id'];
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $num_per_page = 6 * $page;
    $start = 0;
    $list_post = get_list_post_by_id_category($start, $num_per_page, $id);
    //show_array($list_post);
    $data['list_post'] = $list_post;
    $data['page'] = $page;
    $data['start'] = $start;
    #lấy danh sách tag và categories
    $list_categories = get_list_categories();
    $list_tag = get_list_tag();
    //show_array($list_categories);
    $data['list_categories'] = $list_categories;
    $data['list_tag'] = $list_tag;
    //load trên view
    load_view('category', $data);
}

function tagAction() {
    $id=$_GET['id'];
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $num_per_page = 6 * $page;
    $start = 0;
    $list_post = get_list_post_by_id_tag($start, $num_per_page, $id);
    //show_array($list_post);
    $data['list_post'] = $list_post;
    $data['page'] = $page;
    $data['start'] = $start;
    #lấy danh sách tag và categories
    $list_categories = get_list_categories();
    $list_tag = get_list_tag();
    //show_array($list_categories);
    $data['list_categories'] = $list_categories;
    $data['list_tag'] = $list_tag;
    //load trên view
    load_view('tag', $data);
}

function likeAction() {
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $num_per_page = 6 * $page;
    $start = 0;
    $list_post = get_list_post_by_like($start, $num_per_page);
    //show_array($list_post);
    $data['list_post'] = $list_post;
    $data['page'] = $page;
    $data['start'] = $start;
    #lấy danh sách tag và categories
    $list_categories = get_list_categories();
    $list_tag = get_list_tag();
    //show_array($list_categories);
    $data['list_categories'] = $list_categories;
    $data['list_tag'] = $list_tag;
    //load trên view
    load_view('like', $data);
}

function total_starAction() {
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $num_per_page = 6 * $page;
    $start = 0;
    $list_post = get_list_post_by_total_star($start, $num_per_page);
    //show_array($list_post);
    $data['list_post'] = $list_post;
    $data['page'] = $page;
    $data['start'] = $start;
    #lấy danh sách tag và categories
    $list_categories = get_list_categories();
    $list_tag = get_list_tag();
    //show_array($list_categories);
    $data['list_categories'] = $list_categories;
    $data['list_tag'] = $list_tag;
    //load trên view
    load_view('total_star', $data);
}

function topAction(){
    $top_user=get_list_top();
    $data['top_user'] = $top_user;
    load_view('top',$data);
}