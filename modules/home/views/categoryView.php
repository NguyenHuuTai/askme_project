<?php
get_header();
?>
<main>
    <nav class="nav">
        <div class="nav-categories">
            <div class="dropdown nav-categories__select">
                <button class="btn btn-secondary dropdown-toggle nav-dropdown-btn" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Trang chủ
                </button>
                <div class="dropdown-menu nav-dropdown-list nav-dropdown-list-categories"
                     aria-labelledby="dropdownMenuButton">
                    <ul class="row">

                        <li class="col-6"><a href="#" class="category">hello</a></li>
                    </ul>
                </div>
            </div>
            <div class="dropdown nav-categories__select">
                <button class="btn btn-secondary dropdown-toggle nav-dropdown-btn" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Danh mục
                </button>
                <div class="dropdown-menu nav-dropdown-list nav-dropdown-list-categories"
                     aria-labelledby="dropdownMenuButton">
                    <ul class="row">
                        <?php
                        if (!empty($list_categories)) {
                            foreach ($list_categories as $category) {
                                ?>
                                <li class="col-6"><a href="?mod=home&controller=index&action=category&id=<?php echo $category['cat_id']?>" class="category"><?php echo $category['name'] ?></a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="dropdown nav-categories__select">
                <button class="btn btn-secondary dropdown-toggle nav-dropdown-btn" type="button"
                        id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Tags
                </button>
                <div class="dropdown-menu nav-dropdown-list nav-dropdown-list-tags"
                     aria-labelledby="dropdownMenuButton">
                    <ul class="row">
                        <?php
                        if (!empty($list_tag)) {
                            foreach ($list_tag as $tag) {
                                ?>
                                <li class="col-6"><a href="?mod=home&controller=index&action=tag&id=<?php echo $tag['tag_id']?>" class="category"><?php echo $tag['tag_name'] ?></a></li>
                                <?php
                            }
                        }
                        ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="nav-menu">
            <ul>
                <li class="active"><a href="#">Mới nhất </a></li>
                <li><a href="?mod=home&controller=index&action=like">Nhiều like nhất </a></li>
                <li><a href="?mod=home&controller=index&action=total_star">Đánh giá cao nhất </a></li>
                <li><a href="?mod=home&controller=index&action=top">Bảng xếp hạng thành viên </a></li>
            </ul>
        </div>
    </nav>
    <div class="posts">
        <div class="posts__head">
            <div class="posts__topic">Tiêu đề</div>
            <div class="posts__category">Danh mục</div>
            <div class="posts__users">Tag</div>
            <div class="posts__replies">Đánh giá</div>
            <div class="posts__views">Lượt thích</div>
        </div>
        <div class="posts__body">
            <?php
            if (!empty($list_post)) {
                $t = $start;
                foreach ($list_post as $post) {
                    $t++;
                    ?>
                    <div class="posts__item">
                        <div class="posts__section-left">
                            <div class="posts__topic">
                                <div class="posts__content">
                                    <a href="<?php echo "?mod=post&controller=index&action=detail&id=" . $post['post_id'] ?>">
                                        <h3><?php echo $post['title'] ?></h3>
                                    </a>
                                </div>
                            </div>
                            <div class="posts__category"><a href="#" class="category"><i class="bg-368f8b"></i><?php echo $post['name'] ?></a>
                            </div>
                        </div>
                        <div class="posts__section-right">
                            <div class="posts__users">

                                <a href="#" class="avatar"><?php echo $post['tag_name'] ?> </a>

                            </div>
                            <div class="posts__replies"><?php echo $post['total_star'] ?></div>
                            <div class="posts__views"><?php echo $post['total_like'] ?></div>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
        <?php echo get_add_pagging($page, "?mod=home&action=index") ?>
    </div>
</main>
<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="public/js/main.js"></script>
</body>

</html>

