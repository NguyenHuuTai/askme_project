<?php
get_header();
?>

<main>
    <div class="top-user__title">
        <h2>BẢNG XẾP HẠNG THÀNH VIÊN</h2>
        <span>(Tổng số bài viết đã tạo và trả lời)</span>
    </div>
    <div class="row top-user__content">
        <div class="col-xl-4">
            <div class="top-user__item">
                <h3>TOP TUẦN</h3>
                <ul>
                    <?php
                    if (!empty($top_user)) {
                        $t=0;
                        foreach ($top_user as $user) {
                            $t++;
                            ?>
                            <li>
                                <span class="<?php if($t==1){echo "svg-icon icon-medal-1";}if($t==2){echo "svg-icon icon-medal-2";}if($t==3){echo "svg-icon icon-medal-3";}else{echo'';}?>"><?php echo $t; ?></span>
                                <span class="fw-500" ><a style="color:black;" href="?mod=users&controller=index&action=member&id=<?php echo $user['id'] ?>"><?php echo $user['username'] ?></a></span>
                                <span><?php echo $user['total_top'] ?> bài viết</span>
                            </li>
                            <?php
                        }
                    }
                    ?>

                    
                </ul>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="top-user__item">
                <h3>TOP THÁNG</h3>
                <ul>
                    <?php
                    if (!empty($top_user)) {
                        $t=0;
                        foreach ($top_user as $user) {
                            $t++;
                            ?>
                            <li>
                                <span class="<?php if($t==1){echo "svg-icon icon-medal-1";}if($t==2){echo "svg-icon icon-medal-2";}if($t==3){echo "svg-icon icon-medal-3";}else{echo'';}?>"><?php echo $t; ?></span>
                                <span class="fw-500" ><a style="color:black;" href="?mod=users&controller=index&action=member&id=<?php echo $user['id'] ?>"><?php echo $user['username'] ?></a></span>
                                <span><?php echo $user['total_top'] ?> bài viết</span>
                            </li>
                            <?php
                        }
                    }
                    ?>
            </div>
        </div>
        <div class="col-xl-4">
            <div class="top-user__item">
                <h3>TẤT CẢ THỜI GIAN</h3>
                <ul>
                    <?php
                    if (!empty($top_user)) {
                        $t=0;
                        foreach ($top_user as $user) {
                            $t++;
                            ?>
                            <li>
                                <span class="<?php if($t==1){echo "svg-icon icon-medal-1";}if($t==2){echo "svg-icon icon-medal-2";}if($t==3){echo "svg-icon icon-medal-3";}else{echo'';}?>"><?php echo $t; ?></span>
                                <span class="fw-500" ><a style="color:black;" href="?mod=users&controller=index&action=member&id=<?php echo $user['id'] ?>"><?php echo $user['username'] ?></a></span>
                                <span><?php echo $user['total_top'] ?> bài viết</span>
                            </li>
                            <?php
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </div>
</main>

<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="public/js/main.js"></script>
</body>

</html>