<?php

function search_post_by_title($title){
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` WHERE `title` LIKE ('%{$title}%') ORDER BY `post`.`post_id`");
    return $result;
}

function get_list_post($start, $num_per_page = 10, $where = "`post`.`status`='1'") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` {$where} ORDER BY `post`.`post_id` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}

function get_list_categories() {
    $result = db_fetch_array("SELECT * FROM `categories`");
    return $result;
}

function get_list_tag() {
    $result = db_fetch_array("SELECT * FROM `tag`");
    return $result;
}

//lấy dữ liệu theo id của category
function get_list_post_by_id_category($start, $num_per_page = 10,$id ){
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` WHERE `post`.`status`='1' AND `post`.`cat_id`='{$id}' ORDER BY `post`.`post_id` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}
//lấy dữ liệu theo id của tag
function get_list_post_by_id_tag($start, $num_per_page = 10,$id){
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` WHERE `post`.`status`='1' AND `post`.`tag_id`='{$id}' ORDER BY `post`.`post_id` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}

//lấy dữ liệu sắp xếp theo like
function get_list_post_by_like($start, $num_per_page = 10, $where = "`post`.`status`='1'") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` {$where} ORDER BY `post`.`total_like` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}

//lấy dữ liệu sắp xếp theo đánh giá
function get_list_post_by_total_star($start, $num_per_page = 10, $where = "`post`.`status`='1'") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` {$where} ORDER BY `post`.`total_star` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}

//lấy dữ liệu bảng xếp hạng
function get_list_top(){
    $result = db_fetch_array("SELECT * FROM `users` ORDER BY `users`.`total_top` DESC LIMIT 0, 10");
    return $result;
}