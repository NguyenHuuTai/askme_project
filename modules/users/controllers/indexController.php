
<?php

function construct() {
//    echo "DÙng chung, load đầu tiên";
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

//Đăng nhập và đăng kí
function regLoginAction() {
    global $error, $username, $password, $username1, $password1, $email, $badges_id;
    if (isset($_POST['btn-reg'])) {
        $badges_id = 2;
        $error = array();
        #Kiểm tra email
        if (empty($_POST['email'])) {
            $error['email'] = 'Không được để email';
        } else {
            if (!is_email($_POST['email'])) {
                $error['email'] = "Tên email không đúng định dạng ";
            } else {
                $email = $_POST['email'];
            }
        }
        #Kiểm tra username
        if (empty($_POST['username'])) {
            $error['username'] = 'Không được để trống tên đăng nhập ';
        } else {
            if (!is_username($_POST['username'])) {
                $error['username'] = "Tên đăng nhập không đúng định dạng ";
            } else {
                $username = $_POST['username'];
            }
        }
        #Kiểm tra passwword
        if (empty($_POST['password'])) {
            $error['password'] = 'Không được để trống mật khẩu ';
        } else {
            if (!is_password($_POST['password'])) {
                $error['password'] = "Mật khẩu không đúng định dạng ";
            } else {
                $password = md5($_POST['password']);
            }
        }

        if (empty($_POST['confirm_password'])) {
            $error['confirm_password'] = 'Không được để trống mật khẩu xác nhận';
        } else {
            if (!is_password($_POST['confirm_password'])) {
                $error['confirm_password'] = "Mật khẩu không đúng định dạng ";
            }
            if (!compare_password($_POST['confirm_password'], $_POST['password'])) {
                $error['confirm_password'] = "Mật khẩu không giống nhau ";
            }
        }
        $registerArt = time();
        #Kết luận
        if (empty($error)) {
            if (!user_exists($email, $username)) {
                $data = array(
                    'username' => $username,
                    'password' => $password,
                    'email' => $email,
                    'registerArt' => $registerArt,
                    'badges_id' => $badges_id
                );
                add_user($data);
                //Thông báo
                redirect('?mod=users&action=regLogin');
            } else {
                $error['account'] = "Email hoặc Username đã tồn tại ";
            }
        }
    }
    if (isset($_POST['btn-login'])) {
        $error = array();
        #Kiểm tra username
        if (empty($_POST['username'])) {
            $error['username1'] = 'Không được để trống tên đăng nhập ';
        } else {
            if (!is_username($_POST['username'])) {
                $error['username1'] = "Tên đăng nhập không đúng định dạng ";
            } else {
                $username1 = $_POST['username'];
            }
        }
        #Kiểm tra passwword
        if (empty($_POST['password'])) {
            $error['password1'] = 'Không được để trống mật khẩu ';
        } else {
            if (!is_password($_POST['password'])) {
                $error['password1'] = "Mật khẩu không đúng định dạng ";
            } else {
                $password1 = md5($_POST['password']);
            }
        }
        #kết luận
        if (empty($error)) {
            if (check_login($username1, $password1)) {
                //lấy image trong user
                $image = get_user_by_username($username1);

                //lưu trữ phiên đăng nhập
                $_SESSION['is_login'] = true;
                $_SESSION['user_login'] = $username1;
                $_SESSION['admin'] = "2";
                $_SESSION['image'] = $image['avatar'];
                show_array($_SESSION);
                //Chuyển hướng vào bên trong hệ thống
                redirect('?mod=home&controller=index');
            } else {
                $error['account1'] = 'Tên đăng nhập hoặc mật khẩu không tồn tại ';
            }
        }
    }
    load_view('regLogin');
}

//đăng xuất
function logoutAction() {
    $last_login = time();
    $data = array(
        'lastLogin' => $last_login,
    );
    update_last_login(user_login(), $data);
    unset($_SESSION['is_login']);
    unset($_SESSION['user_login']);
    unset($_SESSION['admin']);
    unset($_SESSION['image']);
    redirect('?mod=home');
}

//Thông tin cá nhân
function indexAction() {
    if (!is_login()) {
        redirect('?mod=users&action=regLogin');
    } else {
        $user_detail = get_user_by_username(user_login());
        $total_post_user = get_total_post($user_detail['id']);
        $total_reply_user = get_total_reply($user_detail['id']);
        $total_top = $total_post_user['total_post'] + $total_reply_user['total_reply'];
        $data = array(
            'total_top' => $total_top,
        );
        update_total_top(user_login(), $data);
        $data['user_detail'] = $user_detail;
        $data['total_post_user'] = $total_post_user;
        $data['total_reply_user'] = $total_reply_user;
        load_view('index', $data);
    }
}

//lịch sử hoạt động
function historyAction() {
    if (!is_login()) {
        redirect('?mod=users&action=regLogin');
    } else {
        $user_detail = get_user_by_username(user_login());
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $num_per_page = 4 * $page;
        $start = 0;
        $list_post_user = get_list_user_post_history($start, $num_per_page, $user_detail['id']);
        $data['page'] = $page;
        $data['start'] = $start;
        $data['list_post_user'] = $list_post_user;
        load_view('history_post', $data);
    }
}

//lịch sử reply
function history_replyAction() {
    if (!is_login()) {
        redirect('?mod=users&action=regLogin');
    } else {
        $user_detail = get_user_by_username(user_login());
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        $num_per_page = 4 * $page;
        $start = 0;
        $list_reply_user = get_list_user_reply_history($start, $num_per_page, $user_detail['id']);
        $data['page'] = $page;
        $data['start'] = $start;
        //show_array($list_reply_user);
        $data['list_reply_user'] = $list_reply_user;
        load_view('history_reply', $data);
    }
}

//cập nhật thông tin
function updateAction() {
    global $error, $avatar, $fullname, $gender, $date_of_birth, $phone_number, $description, $address;
    if (isset($_POST['btn-update'])) {
        $error = array();
        if (empty($_POST['fullname'])) {
            $error['fullname'] = 'Không được để trống họ và tên';
        } else {
            $fullname = $_POST['fullname'];
        }
        if (empty($_POST['gender'])) {
            $error['gender'] = 'Không được để trống giới tính';
        } else {
            $gender = $_POST['gender'];
        }
        if (empty($_POST['date_of_birth'])) {
            $error['date_of_birth'] = 'Không được để trống ngày sinh';
        } else {
            $date_of_birth = $_POST['date_of_birth'];
        }
        if (empty($_POST['phone_number'])) {
            $error['phone_number'] = 'Không được để trống số điện thoại';
        } else {
            $phone_number = $_POST['phone_number'];
        }
        $address = $_POST['address'];
        $description = $_POST['description'];
        #===Xử lý ảnh===
        if ($_FILES['file']['name'] == "") {
            $avatar = $_SESSION['image'];
        } else {
            $upload_dir = 'public/images/';
            #Xử lý upload file ảnh
            $type_allow = array('png', 'jpg', 'gift', 'jpeg');
            $avatar = uploadfile($upload_dir, $_FILES['file'], $type_allow);
        }

        if (empty($error)) {
            $data = array(
                'avatar' => $avatar,
                'fullname' => $fullname,
                'gender' => $gender,
                'date_of_birth' => $date_of_birth,
                'phone_number' => $phone_number,
                'description' => $description,
                'address' => $address
            );
            update_user_login(user_login(), $data);
            //ảnh sau khi chỉnh sửa sẽ được cập nhật lại tàn hệ thống
            $_SESSION['image'] = $avatar;
            //Thông báo
            redirect('?mod=users&controller=index&action=index');
        }
    }
    $user_detail = get_detail_by_username(user_login());
    $data['user_detail'] = $user_detail;
    load_view('update', $data);
}

//cập nhật mật khẩu
function update_passAction() {
    global $error, $password, $new_password, $confirm_password;
    if (isset($_POST['btn-reset'])) {
        $error = array();
        #Kiểm tra passwword
        if (empty($_POST['password'])) {
            $error['password'] = 'Không được để trống mật khẩu ';
        } else {
            if (!is_password($_POST['password'])) {
                $error['password'] = "Mật khẩu không đúng định dạng ";
            } else {
                $password = md5($_POST['password']);
            }
        }

        if (empty($_POST['new_password'])) {
            $error['new_password'] = 'Không được để trống mật khẩu ';
        } else {
            if (!is_password($_POST['new_password'])) {
                $error['new_password'] = "Mật khẩu không đúng định dạng ";
            } else {
                $new_password = md5($_POST['new_password']);
            }
        }

        if (empty($_POST['confirm_password'])) {
            $error['confirm_password'] = 'Không được để trống mật khẩu xác nhận';
        } else {
            if (!is_password($_POST['confirm_password'])) {
                $error['confirm_password'] = "Mật khẩu không đúng định dạng ";
            }
            if (!compare_password($_POST['confirm_password'], $_POST['new_password'])) {
                $error['confirm_password'] = "Mật khẩu không giống nhau ";
            }
        }
        #Kết luận

        if (empty($error)) {
            if (check_password(user_login(), $password)) {
                $data = array(
                    'password' => $new_password,
                );
                update_user_password(user_login(), $data);
                //Thông báo
                redirect('?mod=users&controller=index&action=index');
            } else {
                $error['account'] = 'Mật khẩu của bạn không đúng';
            }
        }
    }
    load_view('reset');
}

function memberAction() {
    $id=$_GET['id'];
    $user_detail = get_user_by_id($id);
    $total_post_user = get_total_post($id);
    $total_reply_user = get_total_reply($id);
    $data['user_detail'] = $user_detail;
    $data['total_post_user'] = $total_post_user;
    $data['total_reply_user'] = $total_reply_user;
    load_view('detail', $data);
}
