<?php
get_header();
?>

<main class="edit-password">
    <div class="row">
        <div class="col-xl-3">
            <div class="sidebar">
                <ul>
                    <li><a href="?mod=users&controller=index&action=index"><i class="far fa-user-circle"></i> Thông tin cá nhân</a></li>
                    <li><a href="?mod=users&controller=index&action=history"><i class="fas fa-history"></i> Lịch sử hoạt động</a></li>
                    <!-- m check session cua user neu nguoi khac xem thi khong co dong chinh sua-->
                    <li><a href="?mod=users&controller=index&action=update"><i class="fas fa-cog"></i> Chỉnh sửa thông tin cá nhân</a>
                    <li><a href="?mod=users&controller=index&action=update_pass"><i class="fas fa-unlock-alt"></i> Chỉnh sửa mật khẩu</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xl-9" style="background-color: #edf5fa;">
            <form method="POST"action="" name="edit-password-form" class="edit-password-form">
                <div class="old-password">
                    <p>Nhập mật khẩu hiện tại</p>
                    <input type="password" name="password">
                    <?php echo form_error('password') ?>
                </div>
                <div class="new-password">
                    <p>Nhập mật khẩu mới</p>
                    <input type="password" name="new_password">
                    <?php echo form_error('new_password') ?>
                </div>
                <div class="repeat-password">
                    <p>Nhập lại mật khẩu mới</p>
                    <input name="confirm_password" type="password">
                    <?php echo form_error('confirm_password') ?>
                </div>
                <button name="btn-reset" type="submit">Cập nhật</button>
                <?php echo form_error('account') ?>
            </form>
        </div>

    </div>
</div>
</main>

<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
<script src="public/js/update_profile.js" type="text/javascript"></script>
</body>

</html>