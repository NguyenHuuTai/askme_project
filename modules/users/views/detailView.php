<?php
get_header();
?>

<main class="profile">
    <div class="row">
        <div class="col-xl-3">
            <div class="sidebar">
                
            </div>
        </div>
        <div class="col-xl-9" style="background-color: #edf5fa;">
            <div class="user-info">
                <div class="row">
                    <div class="col-xl-2">
                        <img class="user-info__avatar" src="<?php echo $user_detail['avatar'] ?>">
                    </div>
                    <div class="col-xl-8 user-info__summary">
                        <p class="user-info__username"><?php echo $user_detail['username'] ?></p>
                        <p class="user-info__createAt">Họ và tên: <?php echo $user_detail['fullname'] ?> </p>
                        <p class="user-info__createAt">Tham gia diễn đàn từ ngày: <span> <?php echo date('d/m/Y - H:i:s', $user_detail['registerArt']) ?></span> </p>
                        <p class="user-info__createAt">Lần đăng nhập cuối cùng: <span> <?php if($user_detail['lastLogin']!="1626150483"){echo date('d/m/Y - H:i:s', $user_detail['lastLogin']);}else{echo"Bạn chưa đăng xuất lần nào";} ?></span> </p>
                        <p class="user-info__numberPost">Số bài đã tạo:  <span><?php echo $total_post_user['total_post'] ?></span> lần</p>
                        <p class="user-info__numberReply">Đã trả lời :  <span><?php echo $total_reply_user['total_reply'] ?></span> lần</p>
                        <p class="user-info__badges">Danh hiệu đạt được: <span><?php echo $user_detail['name'] ?></span></p>
                    </div>
                </div>
                <div class="row user-info__detail">
                    <div class="col-xl-12">
                        <h2>Thông tin khác</h2>
                    </div>
                    <div class="col-xl-12">
                        <p class="user-info__sex">Giới tính: <?php echo $user_detail['gender'] ?></p>
                    </div>
                    <div class="col-xl-12">
                        <p class="user-info__birthday">Ngày sinh: <span><?php echo $user_detail['date_of_birth'] ?></span></p>
                    </div>
                    <div class="col-xl-12">
                        <p class="user-info__email">Email: <span><?php echo $user_detail['email'] ?></span></p>
                    </div>
                    <div class="col-xl-12">
                        <p class="user-info__address">Địa chỉ: <span><?php echo $user_detail['address'] ?></span></p>
                    </div>
                    <div class="col-xl-12">
                        <p class="user-info__description">Giới thiệu: <span><?php echo $user_detail['description'] ?></span>
                        </p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>

<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
</body>

</html>