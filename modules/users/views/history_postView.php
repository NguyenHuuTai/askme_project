<?php
get_header();
?>

<main class="main-history">
    <div class="row">
        <div class="col-xl-3">
            <div class="sidebar">
                <ul>
                    <li><a href="?mod=users&controller=index&action=index"><i class="far fa-user-circle"></i> Thông tin cá nhân</a></li>
                    <li><a href="?mod=users&controller=index&action=history"><i class="fas fa-history"></i> Lịch sử hoạt động</a></li>
                    <!-- m check session cua user neu nguoi khac xem thi khong co dong chinh sua-->
                    <li><a href="?mod=users&controller=index&action=update"><i class="fas fa-cog"></i> Chỉnh sửa thông tin cá nhân</a>
                    <li><a href="?mod=users&controller=index&action=update_pass"><i class="fas fa-unlock-alt"></i> Chỉnh sửa mật khẩu</a></li>
                </ul>
                </ul>
            </div>
        </div>
        <div class="col-xl-9" style="background-color: #edf5fa;">
            <h2 class="history-h2">Lịch sử hoạt động</h2>
            <h2 class="history-option"><a href="?mod=users&controller=index&action=history">Lịch sử bài đăng</a></h2>
            <h2 class="history-option"><a href="?mod=users&controller=index&action=history_reply">Lịch sử bài trả lời</a></h2>
            <?php if (empty($list_post_user)) { ?>
                <h1>Bạn chưa có bài viết nào hãy tạo bài viết mới <a href="?mod=post&action=add">tại đây</a></h1> 
                <?php
            } else {
                $t = $start;
                foreach ($list_post_user as $post) {
                    $t++;
                    ?>

                    <div class="history-post">
                        <h4 class="history-para">Bạn đã tạo bài viết:</h4>
                        <div class="history-title history-flex">
                            <h4>Tiêu đề: </h4>
                            <a href="<?php echo "?mod=post&controller=index&action=detail&id=" . $post['post_id'] ?>"><span><?php echo $post['title'] ?></span></a>
                        </div>
                        <div class="history-time history-flex">
                            <h4>Thời gian tạo:</h4> <span> <?php echo date('d/m/Y - H:i:s', $post['time']); ?></span>
                        </div>
                        <div class="history-content">
                            <h4>Nội dung:</h4>
                            <span><?php echo $post['content'] ?></span>
                        </div>
                    </div>
                    <?php
                }
                ?>
               
                <?php echo get_add_pagging($page, "?mod=users&action=history") ?>
            <?php } ?>
        </div>
    </div>
</main>

<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
</body>

</html>