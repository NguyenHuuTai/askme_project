<?php
get_header();
?>
<main class="edit-profile">
    <div class="row">
        <div class="col-xl-3">
            <div class="sidebar">
                <ul>
                    <li><a href="?mod=users&controller=index&action=index"><i class="far fa-user-circle"></i> Thông tin cá nhân</a></li>
                    <li><a href="?mod=users&controller=index&action=history"><i class="fas fa-history"></i> Lịch sử hoạt động</a></li>
                    <!-- m check session cua user neu nguoi khac xem thi khong co dong chinh sua-->
                    <li><a href="?mod=users&controller=index&action=update"><i class="fas fa-cog"></i> Chỉnh sửa thông tin cá nhân</a>
                    <li><a href="?mod=users&controller=index&action=update_pass"><i class="fas fa-unlock-alt"></i> Chỉnh sửa mật khẩu</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xl-9" style="background-color: #edf5fa;">
            <form method="POST" enctype="multipart/form-data" class="edit-profile-form">
                <div class="edit-avatar">
                    <h2 class="edit-avt-title">Chỉnh sửa ảnh đại diện</h2>
                    <div class="avatar-wrapper">
                        <img class="profile-pic" src="<?php echo $user_detail['avatar'] ?>" />
                        <div class="upload-button">
                            <i class="fa fa-arrow-circle-up" aria-hidden="true"></i>
                        </div>

                        <input type="file" name="file" class="file-upload"/>
                    </div>
                    <label class="btn-upload" for="file-upload">Chọn ảnh mới</label>
                </div>

                <div class="row">
                    <div class="col-xl-6">
                        <div class="edit-fullname">
                            <p>Họ và tên</p>
                            <input name="fullname" type="text" value="<?php echo $user_detail['fullname'] ?>">
                             <?php echo form_error('fullname') ?>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="edit-sex">
                            <p>Giới tính</p>
                            <select name="gender">
                                <option <?php if(isset($user_detail['gender']) && $user_detail['gender']=='Nam') echo "selected=selected"; ?> value="Nam">Nam</option>
                                <option <?php if(isset($user_detail['gender']) && $user_detail['gender']=='Nữ') echo "selected=selected"; ?> value="Nữ">Nữ</option>
                            </select>
                            <?php echo form_error('gender') ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-6">
                        <div class="edit-phonenumber">
                            <p>Số điện thoại</p>
                            <input name="phone_number" type="text" value="<?php echo $user_detail['phone_number'] ?>">
                            <?php echo form_error('phone_number') ?>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="edit-birthday">
                            <p>Ngày sinh</p>
                            <input name="date_of_birth" type="date" value="<?php echo $user_detail['date_of_birth'] ?>">
                            <?php echo form_error('date_of_birth') ?>
                        </div>
                    </div>
                </div>
                <div class="edit-address">
                    <p>Địa chỉ</p>
                    <input name="address" type="text" value="<?php echo $user_detail['address'] ?>">
                    <?php echo form_error('address') ?>
                </div>

                <div class="edit-desc">
                    <p>Giới thiệu</p>
                    <textarea name="description" > <?php echo $user_detail['description'] ?> </textarea>
                </div>

                <button name="btn-update" type="submit">Cập nhật</button>
            </form>
        </div>

    </div>
</div>
</main>

<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
<script src="public/js/update_profile.js" type="text/javascript"></script>
</body>

</html>