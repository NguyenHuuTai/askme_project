<?php

function compare_password($pass1, $pass2) {
    if ($pass1 == $pass2) {
        return true;
    }
    return false;
}

function update_user_login($username, $data) {
    return db_update('users', $data, "`username` = '{$username}'");
}

function update_user_password($username, $data) {
    return db_update('users', $data, "`username` = '{$username}'");
}

function update_last_login($username, $data) {
    return db_update('users', $data, "`username` = '{$username}'");
}

function update_total_top($username, $data) {
    return db_update('users', $data, "`username` = '{$username}'");
}

function get_user_by_username($username) {
    $item = db_fetch_row("SELECT * FROM `users` JOIN `badges` ON `badges`.`badges_id`=`users`.`badges_id` WHERE `username` = '{$username}'");
    if (!empty($item))
        return $item;
}

function get_user_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `users` JOIN `badges` ON `badges`.`badges_id`=`users`.`badges_id` WHERE `id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_detail_by_username($username) {
    $item = db_fetch_row("SELECT * FROM `users` WHERE `username` = '{$username}'");
    if (!empty($item))
        return $item;
}


function check_login($username, $password) {
    $check_user = db_num_rows("SELECT * FROM `users` WHERE `password` = '{$password}' AND `username` = '{$username}'");
    echo $check_user;
    if ($check_user > 0)
        return true;
    return false;
}

function check_password($username, $password) {
    $check_user = db_num_rows("SELECT * FROM `users` WHERE `password` = '{$password}' AND `username` = '{$username}'");
    echo $check_user;
    if ($check_user > 0)
        return true;
    return false;
}

function info_user($label = 'id') {
    $user_login = $_SESSION['user_login'];
    $user = db_fetch_array("SELECT * FROM `users` WHERE `email` = '{$user_login}'");
    return $user[$label];
}

function add_user($data) {
    return db_insert('users', $data);
}

function user_exists($email, $username) {
    $check_user = db_num_rows("SELECT * FROM `users` WHERE `email` = '{$email}' OR `username` = '{$username}'");
    echo $check_user;
    if ($check_user > 0)
        return true;
    return false;
}

function get_total_post($id){
    $count_post = db_fetch_row("SELECT COUNT(post_id) AS 'total_post' FROM `post` WHERE `author_id` = '{$id}'");
    return $count_post;
}

function get_total_reply($id){
    $count_post = db_fetch_row("SELECT COUNT(reply_id) AS 'total_reply' FROM `replies` WHERE `user_id` = '{$id}'");
    return $count_post;
}

//===UPLOAD ẢNH =====
function uploadfile($upload_dir, $image, $type_allow) {
    $error = array();
    //đường dẫn của file sau khi upload
    $upload_file = $upload_dir . $image['name'];
    $type = pathinfo($image['name'], PATHINFO_EXTENSION);
    #kiểm tra đuôi file của thuộc vào lại cho phép hay không
    if (!in_array(strtolower($type), $type_allow)) {
        $error['type'] = "Chỉ được upload các loại file có định dạng sau: png, jpg, gift, jpeg. ";
    } else {
        #kiểm tra file upload có kích thước cho phép < 20mb=29000000 byte
        $file_size = $image['size'];
        if ($file_size > 29000000) {
            $error['size'] = "Chỉ được upload file bé hơn 20MB";
        }
        #Kiểm tra cùng tên trong hệ thống
        if (file_exists($upload_file)) {
            //$error['file_exists'] = "File đã tồn tại trong hệ thống";
            //===================================================
            //=========XỬ LÝ ĐỔI TÊN FILE TỰ ĐỘNG=============
            //================================================
            # Bước 1:Tạo file mơi
            $file_name = pathinfo($image['name'], PATHINFO_FILENAME);
            $new_file_name = $file_name . ' - Copy.';
            $new_upload_file = $upload_dir . $new_file_name . $type;
            #Bước 2: kiểm tra tiếp tục
            $k = 1;
            while (file_exists($new_upload_file)) {
                $new_file_name = $file_name . " - Copy({$k}).";
                $k++;
                $new_upload_file = $upload_dir . $new_file_name . $type;
            }
            #Bước 3: kết luận
            $upload_file = $new_upload_file;
        }
    }
    #kết luận
    if (empty($error)) {
        if (move_uploaded_file($_FILES['file']['tmp_name'], $upload_file)) {
            return $upload_file;
        } else {
            $error['upload'] = "upload file không thành công";
            return $error;
        }
    } else {
        return $error;
    }
}

//lấy lịch sử các bài viết

function get_list_user_post_history($start, $num_per_page = 10, $id) {
    $result = db_fetch_array("SELECT * FROM `post` WHERE `post`.`status`='1' AND `post`.`author_id`='{$id}'  ORDER BY `post`.`time` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}

function get_list_user_reply_history($start, $num_per_page = 10, $id) {
    $result = db_fetch_array("SELECT * FROM `replies` JOIN `post` ON `replies`.`post_id`=`post`.`post_id` WHERE `replies`.`reply_status`='1' AND `replies`.`user_id`='{$id}'  ORDER BY `replies`.`createdAt` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}