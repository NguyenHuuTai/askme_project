<?php
get_header();
?>
<main>
    <div class="topics">
        <div class="topics__heading">
            <h2 class="topics__heading-title"><?php echo $detail_post_id['title'] ?></h2>
            <div class="topics__heading-info">
                <div>
                    <span class="topics__heading-category">Category:</span>
                    <a href="#" class="category"><?php echo $detail_post_id['name'] ?></a>
                </div>

                <div class="tags">
                    <span class="topics__heading-tags"> Tags:</span>
                    <a href="#" class="bg-4f80b0"><?php echo $detail_post_id['tag_name'] ?></a>
                </div>
            </div>
        </div>
        <div class="topics__body">
            <div class="topics__content">
                <div class="topic">
                    <div class="topic__head">
                        <div class="topic__avatar">
                            <a href="?mod=users&controller=index&action=member&id=<?php echo $detail_post_id['id'] ?>" class="avatar"><img src="<?php echo $detail_post_id['avatar'] ?>" alt="avatar"></a>
                        </div>
                        <div class="topic__caption">
                            <div class="topic__name">
                                <a href="?mod=users&controller=index&action=member&id=<?php echo $detail_post_id['id'] ?>"><?php echo $detail_post_id['username'] ?></a>
                            </div>
                            <div class="topic__date">
                                <i class="far fa-clock"></i>
                                <span><?php echo date('d/m/Y - H:i:s', $detail_post_id['time']); ?></span>
                            </div>
                        </div>
                    </div>
                    <div class="topic__content">
                        <div class="topic__text">
                            <p><?php echo $detail_post_id['content'] ?></p>

                        </div>
                        <div class="topic__footer">
                            <div class="topic__footer-likes">
                                <div>
                                    <a  id="add_like_post" data-id="<?php echo $detail_post_id['post_id'] ?>">
                                        <i class="far fa-thumbs-up" ></i>
                                        <span id="post_total_like-<?php echo $detail_post_id['post_id'] ?>"><?php echo $detail_post_id['total_like'] ?></span>
                                    </a>

                                </div>
                                <div class="topic__footer-star">
                                    <i class="fas fa-star"></i>
                                    <span id="post_total_star-<?php echo $detail_post_id['post_id'] ?>"><?php echo $detail_post_id['total_star'] ?></span>
                                </div>
                            </div>
                            <div class="topic__footer-report">
                                <div class="mark-btn">
                                    <form method="POST" class="mark-btn__input" id="give_point" data-id="<?php echo $detail_post_id['post_id'] ?>">
                                        <span>Chọn điểm: </span>
                                        <input value="1" min="1" max="5" step="1" type="number" id="point_post"
                                               style="width:40px" />
                                        <button type="submit">OK</button>
                                    </form>

                                    <button class="mark-btn__txt" onclick="showMarkInput(0)">
                                        <i class="fas fa-star-of-david"></i>
                                        <span>Đánh giá</span>
                                    </button>

                                </div>
                                <div>
                                    <button class="report-btn" onclick="showFormReport(0)">
                                        <i class="far fa-flag"></i>
                                        <span>Báo cáo</span>
                                    </button>
                                </div>
                            </div>
                            <div class="topic__report">
                                <form action="" method="POST" class="topics__report-form" id="report_post" data-id-post="<?php echo $detail_post_id['post_id'] ?>">
                                    <p class="report-form__title">Thông báo bài xấu</p>
                                    <div class="report-form__select">
                                        <span>Lý do: </span>
                                        <select id="report_content">
                                            <option value="0">-- Chọn lý do --</option>
                                            <option value="1">Nội dung bài viết chứa Tiếng Việt không dấu</option>
                                            <option value="2">Nội dung bài viết vi phạm nội quy của diễn đàn</option>
                                            <option value="3">Spam/Quảng cáo trong nội dung bài viết</option>
                                        </select>
                                    </div>
                                    <div class="report-form__other">
                                        <span>Khác:</span>
                                        <textarea placeholder="Nhập lí do khác tại đây" id="report_external"></textarea>
                                    </div>
                                    <div class="report-form__button">
                                        <button type="button" onclick="CancelReport(0)">Hủy</button>
                                        <button type="submit">Gửi</button>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                if (!empty($list_replies)) {
                    $t = 0;
                    foreach ($list_replies as $reply) {
                        $t++;
                        ?>
                        <div class="topic">
                            <div class="topic__head">
                                <div class="topic__avatar">
                                    <a href="?mod=users&controller=index&action=member&id=<?php echo $reply['id'] ?>" class="avatar"><img src="<?php echo $reply['avatar'] ?>" alt="avatar"></a>
                                </div>
                                <div class="topic__caption">
                                    <div class="topic__name">
                                        <a href="?mod=users&controller=index&action=member&id=<?php echo $reply['id'] ?>"><?php echo $reply['username'] ?></a>
                                    </div>
                                    <div class="topic__date">
                                        <i class="far fa-clock"></i>
                                        <span><?php echo date('d/m/Y - H:i:s', $reply['createdAt']); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="topic__content">
                                <div class="topic__text">
                                    <p><?php echo $reply['reply_content'] ?></p>

                                </div>
                                <div class="topic__footer">
                                    <div class="topic__footer-likes">
                                        <div>
                                            <a href="" id="add_like_reply" data-id-reply="<?php echo $reply['reply_id'] ?>">
                                                <i class="far fa-thumbs-up"></i>
                                                <span id="reply_total_like-<?php echo $reply['reply_id'] ?>"><?php echo $reply['total_like'] ?></span>
                                            </a>

                                        </div>
                                        <div class="topic__footer-star">
                                            <i class="fas fa-star"></i>
                                            <span  id="reply_total_star-<?php echo $reply['reply_id']?>"><?php echo $reply['total_star'] ?></span>
                                        </div>
                                    </div>
                                    <div class="topic__footer-report">
                                        <div class="mark-btn">
                                            <form method="POST" class="mark-btn__input" id="give_point_reply" data-id-reply="<?php echo $reply['reply_id'] ?>">
                                                <span>Chọn điểm: </span>
                                                <input value="1" min="1" max="5" step="1" type="number" id="point_reply-<?php echo $reply['reply_id'] ?>"
                                                       style="width:40px" />
                                                <button type="submit" >OK</button>
                                            </form>
                                            <button class="mark-btn__txt" onclick="showMarkInput(<?php echo $t; ?>)">
                                                <i class="fas fa-star-of-david"></i>
                                                <span>Đánh giá</span>
                                            </button>

                                        </div>
                                        <div>
                                            <button class="report-btn" onclick="showFormReport(<?php echo $t; ?>)">
                                                <i class="far fa-flag"></i>
                                                <span>Báo cáo</span>
                                            </button>

                                        </div>

                                    </div>
                                    <div class="topic__report">
                                        <form action="" method="POST" class="topics__report-form" id="report_reply" id-reply-report="<?php echo $reply['reply_id'] ?>" >
                                            <p class="report-form__title">Thông báo bài xấu</p>
                                            <div class="report-form__select">
                                                <span>Lý do: </span>
                                                <select id="report_content-<?php echo $reply['reply_id'] ?>">
                                                    <option value="0">-- Chọn lý do --</option>
                                                    <option value="1">Nội dung bài viết chứa Tiếng Việt không dấu</option>
                                                    <option value="2">Nội dung bài viết vi phạm nội quy của diễn đàn</option>
                                                    <option value="3">Spam/Quảng cáo trong nội dung bài viết</option>
                                                </select>
                                            </div>
                                            <div class="report-form__other">
                                                <span>Khác:</span>
                                                <textarea placeholder="Nhập lí do khác tại đây" id="report_external-<?php echo $reply['reply_id'] ?>"></textarea>
                                            </div>
                                            <div class="report-form__button">
                                                <button type="button" onclick="CancelReport(1)">Hủy</button>
                                                <button type="submit">Gửi</button>
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>


            </div>
        </div>
        <form method="POST" class="topics__comment" id="comment_post" data-id-comment="<?php echo $detail_post_id['post_id'] ?>">
            <label >Bình luận</label>
            <textarea name="content" id="comment_content" class="ckeditor"></textarea>
            <?php echo form_error('content') ?>
            <button name="btn-comment" type="submit">Đăng</button>
        </form>
    </div>
</main>


<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
<script src="public/js/post.js" type="text/javascript"></script>
<script src="public/js/jquery-3.6.0.min.js" type="text/javascript"></script>
</body>

</html>