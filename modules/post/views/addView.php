<?php
get_header();
?>
<form method="POST">
    <main>
        <div class="create">
            <div class="create__head">
                <div class="create__title"><img src="public/images/New_Topic.svg" alt="New topic">Create New Thread</div>
                <span>Forum Guidelines</span>
            </div>
            <div class="create__section">
                <label class="create__label" for="title">Thread Title</label>
                <input type="text" class="form-control" name="title" id="title" placeholder="Add here" value="">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="create__section">
                        <label class="create__label" for="category" >Select Category</label>
                        <select id="category" name="cat_id">
                            <?php
                            if (!empty($list_categories)) {
                                foreach ($list_categories as $category) {
                                    ?>
                                    <option value="<?php echo $category['cat_id'] ?>"><?php echo $category['name'] ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="create__section">
                        <label class="create__label" for="category">Select Tag</label>
                        <select id="category" name="tag_id">
                            <?php
                            if (!empty($list_tag)) {
                                foreach ($list_tag as $tag) {
                                    ?>
                                    <option value="<?php echo $tag['tag_id'] ?>"><?php echo $tag['tag_name'] ?></option>
                                    <?php
                                }
                            }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="create__section create__textarea">
                <label class="create__label" for="description">Description</label>
                <textarea name="content" id="description" class="ckeditor"></textarea>
            </div>
            <div class="create__section">
                <label class="create__label" for="tags">Add Tags</label>
                <input type="text" class="form-control" name="tag" id="tags" placeholder="e.g. nature, science">
            </div>
            <div class="create__footer">
                <a href="?mod=home" class="create__btn-cansel btn">Cancel</a>
                <button type="submit" name="btn-add" class="create__btn-create btn btn--type-02">Create Thread</button>
            </div>
        </div>
    </main>
</form>



<?php
get_footer();
?>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
crossorigin="anonymous"></script>
<script src="public/js/main.js"></script>
</body>

</html>