<?php

function get_user($user) {
    $result = db_fetch_row("SELECT * FROM `users` WHERE `username` = '{$user}'");
    return $result;
}

function get_list_categories() {
    $result = db_fetch_array("SELECT * FROM `categories`");
    return $result;
}

function get_list_tag() {
    $result = db_fetch_array("SELECT * FROM `tag`");
    return $result;
}
//hàm thêm 
function add_post($data) {
    return db_insert('post', $data);
}
//hàm thêm report post
function add_report_post($data) {
    return db_insert('report_post', $data);
}
//hàm thêm report reply
function add_report_reply($data) {
    return db_insert('report_reply', $data);
}
//thêm reply
function add_reply($data) {
    return db_insert('replies', $data);
}
//lấy dòng câu hỏi theo id
function get_detail_post_by_id($id){
    $result = db_fetch_row("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` WHERE `post_id`='{$id}' ORDER BY `post`.`post_id`");
    return $result;
}
//hàm lấy danh sách câu trả lời theo id của bài post
function get_list_replies_by_id($id){
    $result = db_fetch_array("SELECT * FROM `replies` JOIN `users` ON `replies`.`user_id`=`users`.`id` WHERE `post_id`='{$id}' AND `replies`.`reply_status`='1'  ORDER BY `replies`.`createdAt` DESC");
    return $result;
}

function get_post_by_id($id){
        $result = db_fetch_row("SELECT * FROM `post` WHERE `post_id`='{$id}'");
    return $result;
}

function update_total_like($id, $data){
    return db_update('post', $data, "`post_id`='{$id}'");
}

function update_total_star($id, $data){
    return db_update('post', $data, "`post_id`='{$id}'");
}
//hàm lấy câu trả lời theo id của bài reply
function get_reply_by_id($id){
        $result = db_fetch_row("SELECT * FROM `replies` WHERE `reply_id`='{$id}'");
    return $result;
}

function update_total_like_reply($id, $data){
    return db_update('replies', $data, "`reply_id`='{$id}'");
}

function update_total_star_reply($id, $data){
    return db_update('replies', $data, "`reply_id`='{$id}'");
}