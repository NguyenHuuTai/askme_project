<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
    load('lib', 'email');
}

//THÊM CÂU HỎI
function addAction() {
    if (!is_login()) {
        redirect('?mod=users&action=regLogin');
    } else {
        $list_categories = get_list_categories();
        $list_tag = get_list_tag();
        $data['list_categories'] = $list_categories;
        $data['list_tag'] = $list_tag;
        $user = get_user(user_login());
        global $error, $title, $cat_id, $tag_id, $author_id, $content, $tag, $time;
        if (isset($_POST['btn-add'])) {
            $error = array();
            if (empty($_POST['title'])) {
                $error['title'] = 'Không được để trống tiêu đề';
            } else {
                $title = $_POST['title'];
            }
            if (empty($_POST['cat_id'])) {
                $error['cat_id'] = 'Không được để trống danh mục';
            } else {
                $cat_id = $_POST['cat_id'];
            }
            if (empty($_POST['tag_id'])) {
                $error['tag_id'] = 'Không được để trống tag';
            } else {
                $tag_id = $_POST['tag_id'];
            }
            if (empty($_POST['content'])) {
                $error['content'] = 'Không được để trống câu hỏi';
            } else {
                $content = $_POST['content'];
            }
            $tag = $_POST['tag'];
            $time = time();
            $author_id = $user['id'];
//        show_array($_POST);
            //show_array($user);
//        echo $time;
            if (empty($error)) {
                $data_1 = array(
                    'title' => $title,
                    'cat_id' => $cat_id,
                    'tag_id' => $tag_id,
                    'author_id' => $author_id,
                    'content' => $content,
                    'tag' => $tag,
                    'time' => $time,
                );
                add_post($data_1);
                //Gửi mail thông báo nếu admin bật chế độ gửi thông báo
                if (isset($_SESSION['auto_email'])) {
                    $link_active = base_url("admin/?mod=post&action=index");
                    $email_content = "<p>Đã có bài mới cần bạn duyêt. Vui lòng nhấn theo đường dẫn này:{$link_active} </p>";
                    send_mail($_SESSION['auto_email'], 'Nguyễn Hữu Tài', 'Bài post mới', $email_content);
                }
                //Thông báo
                redirect('?mod=home&controller=index');
            }
        }
        load_view('add', $data);
    }
}

//Chi tiết câu hỏi
function detailAction() {
    $id = $_GET['id'];
    $detail_post_id = get_detail_post_by_id($id);
    $list_replies = get_list_replies_by_id($id);
    //show_array($list_replies);
    $data['detail_post_id'] = $detail_post_id;
    $data['list_replies'] = $list_replies;
    load_view('detail', $data);
}

//chi tiết comment
function commentAction() {
    $id = $_POST['id'];
    $content = $_POST['content'];
    if (!is_login()) {
        echo "Bạn chưa đăng nhập tài khoản! Vui lòng đăng nhập.";
    } else {
        if ($content == "") {
            echo "Bạn hãy nhấn Đăng thêm lần nữa vì sự cố lần đầu sẽ chưa ghi nhận đc dữ liệu";
        } else {
            $user = get_user(user_login());
            $time = time();
            $data_1 = array(
                'user_id' => $user['id'],
                'post_id' => $id,
                'reply_content' => $content,
                'createdAt' => $time,
            );
            add_reply($data_1);
            echo "Bài viết của bạn đã được ghi nhận và đang chờ được duyệt";
        }
    }
}

//thêm like post
function add_like_postAction() {
    $id = $_POST['id'];
    if (!is_login()) {
        $post_by_id = get_post_by_id($id);
        $new_total_like = $post_by_id['total_like'];
        $data_1 = array(
            'total_like' => $new_total_like,
        );
        update_total_like($id, $data_1);
        echo $new_total_like;
    } else {
        $post_by_id = get_post_by_id($id);
        $new_total_like = $post_by_id['total_like'] + 1;
        $data_1 = array(
            'total_like' => $new_total_like,
        );
        update_total_like($id, $data_1);
        echo $new_total_like;
    }
}

//thêm điểm post
function give_pointAction() {
    $id = $_POST['id'];

    $point_post = $_POST['point_post'];
    if (!is_login()) {
        $post_by_id = get_post_by_id($id);
        $new_total_vote = $post_by_id['total_vote'];
        $new_total_point = $post_by_id['total_point'] + $point_post * 0;
        $new_total_star = number_format($new_total_point / $new_total_vote, 2);
        $data_1 = array(
            'total_star' => $new_total_star,
            'total_vote' => $new_total_vote,
            'total_point' => $new_total_point,
        );
        update_total_star($id, $data_1);
        echo $new_total_star;
    } else {
        $post_by_id = get_post_by_id($id);
        $new_total_vote = $post_by_id['total_vote'] + 1;
        $new_total_point = $post_by_id['total_point'] + $point_post;
        $new_total_star = number_format($new_total_point / $new_total_vote, 2);
        $data_1 = array(
            'total_star' => $new_total_star,
            'total_vote' => $new_total_vote,
            'total_point' => $new_total_point,
        );
        update_total_star($id, $data_1);
        echo $new_total_star;
    }
}

//thêm báo cáo bài post
function report_postAction() {
    $id = $_POST['id'];
    if (!is_login()) {
        echo "Bạn chưa đăng nhập tài khoản! Vui lòng đăng nhập.";
    } else {
        $report_content = $_POST['report_content'];
        $report_external = $_POST['report_external'];
        $user = get_user(user_login());
        $data_1 = array(
            'user_id' => $user['id'],
            'post_id' => $id,
            'report_content' => $report_content,
            'report_external' => $report_external,
        );
        add_report_post($data_1);
        echo "Chúng tôi đã ghi nhận báo cáo bài viết của bạn";
    }
}

//thêm like reply
function add_like_replyAction() {
    $id = $_POST['id'];
    if (!is_login()) {
        $reply_by_id = get_reply_by_id($id);
        $new_total_like_reply = $reply_by_id['total_like'];
        $data_1 = array(
            'total_like' => $new_total_like_reply,
        );
        update_total_like_reply($id, $data_1);
        echo $new_total_like_reply;
    } else {
        $reply_by_id = get_reply_by_id($id);
        $new_total_like_reply = $reply_by_id['total_like'] + 1;
        $data_1 = array(
            'total_like' => $new_total_like_reply,
        );
        update_total_like_reply($id, $data_1);
        echo $new_total_like_reply;
    }
}

//thêm điêm reply
function give_point_replyAction() {
    $id = $_POST['id'];
    $point_reply = $_POST['point_post'];
    if (!is_login()) {
        $reply_by_id = get_reply_by_id($id);
        $new_total_vote = $reply_by_id['total_vote'];
        $new_total_point = $reply_by_id['total_point'] + $point_reply * 0;
        $new_total_star = number_format($new_total_point / $new_total_vote, 2);
        $data_1 = array(
            'total_star' => $new_total_star,
            'total_vote' => $new_total_vote,
            'total_point' => $new_total_point,
        );
        update_total_star_reply($id, $data_1);
        echo $new_total_star;
    } else {
        $reply_by_id = get_reply_by_id($id);
        $new_total_vote = $reply_by_id['total_vote'] + 1;
        $new_total_point = $reply_by_id['total_point'] + $point_reply;
        $new_total_star = number_format($new_total_point / $new_total_vote, 2);
        $data_1 = array(
            'total_star' => $new_total_star,
            'total_vote' => $new_total_vote,
            'total_point' => $new_total_point,
        );
        update_total_star_reply($id, $data_1);
        echo $new_total_star;
    }
}

//thêm báo cáo bài reply
function report_replyAction() {
    $id = $_POST['id'];
    if (!is_login()) {
        echo "Bạn chưa đăng nhập tài khoản! Vui lòng đăng nhập.";
    } else {
        $report_content = $_POST['report_content'];
        $report_external = $_POST['report_external'];
        $user = get_user(user_login());
        $data_1 = array(
            'user_id' => $user['id'],
            'reply_id' => $id,
            'rp_content' => $report_content,
            'rp_external' => $report_external,
        );
        add_report_reply($data_1);
        echo "Chúng tôi đã ghi nhận báo cáo bài viết của bạn";
    }
}
