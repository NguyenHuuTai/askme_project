function showMarkInput(value) {
    var markinput = document.getElementsByClassName("mark-btn__input");
    markinput[value].classList.toggle("toggle");
}

function showFormReport(value) {
    var report_form = document.querySelectorAll(".topic .topic__report");
    report_form[value].classList.toggle("toggle");
}

function CancelReport(value) {
    var report_form = document.querySelectorAll(".topic .topic__report");
    report_form[value].classList.remove("toggle");
}


$(document).ready(function () {
//    ajax like post
    $("a#add_like_post").click(function () {
        var id = $(this).attr('data-id');
        var data = {id: id};
        $.ajax({
            url: '?mod=post&controller=index&action=add_like_post',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                $("#post_total_like-" + id).text(data);

            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
    });
//ajax point post   
    $("form#give_point").submit(function () {
        var id = $(this).attr('data-id');
        var point_post = $('#point_post').val();
        var data = {id: id, point_post: point_post};
        //alert(point_post);
        //alert(id);
        $.ajax({
            url: '?mod=post&controller=index&action=give_point',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                $("#post_total_star-" + id).text(data);

            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
        return false;
    });
//ajax like reply   
    $("a#add_like_reply").click(function () {
        var id = $(this).attr('data-id-reply');
        var data = {id: id};
        $.ajax({
            url: '?mod=post&controller=index&action=add_like_reply',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                $("#reply_total_like-" + id).text(data);

            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
        return false;
    });
//ajax give point reply
    $("form#give_point_reply").submit(function () {
        var id = $(this).attr('data-id-reply');
        var point_post = $("#point_reply-" + id).val();
        var data = {id: id, point_post: point_post};
        //alert(point_post);
        //alert(id);
        $.ajax({
            url: '?mod=post&controller=index&action=give_point_reply',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                $("#reply_total_star-" + id).text(data);

            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
        return false;
    });
    //ajax comment post
    $("form#comment_post").submit(function () {
        var id = $(this).attr('data-id-comment');
        var content = $('textarea#comment_content').val();
        var data = {id: id, content:content};
        //alert(content);
        $.ajax({
            url: '?mod=post&controller=index&action=comment',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                alert(data);
            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
        return false;
    });
    //ajax report reply
    $("form#report_reply").submit(function () {
        var id = $(this).attr('id-reply-report');
        var report_content = $('#report_content-' + id).val();
        var report_external = $('#report_external-' + id).val();
        var data = {id: id, report_content: report_content, report_external: report_external};
        //alert(report_external);
        $.ajax({
            url: '?mod=post&controller=index&action=report_reply',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                alert("Chúng tôi đã ghi nhận báo cáo bài viết của bạn");
            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
        return false;
    });
    //ajax report post
    $("form#report_post").submit(function () {
        var id = $(this).attr('data-id-post');
        var report_content = $('#report_content').val();
        var report_external = $('#report_external').val();
        var data = {id: id, report_content: report_content, report_external: report_external};
        //alert(report_external);
        $.ajax({
            url: '?mod=post&controller=index&action=report_post',
            method: 'POST',
            data: data, //dữ liệu chọn vào
            datatype: 'text',
            success: function (data) {
                alert("Chúng tôi đã ghi nhận báo cáo bài viết của bạn");
            },
            error: function (xhr, ajaxOptions, throwError) {
                alert(xhr.status);
                alert(throwError);
            }
        });
        return false;
    });
});


