<?php
function currency_format($number, $suffix = 'đ'){
    return number_format($number).$suffix;
}

function show_status($status) {
    $list_status = array(
        '0' => 'Chưa duyệt',
        '1' => 'Được duyệt'
    );
    if (array_key_exists($status, $list_status)) {
        return $list_status[$status];
    }
}

function show_role($role) {
    $list_role = array(
        '1' => 'Admin',
        '2' => 'Thành viên',
    );
    if (array_key_exists($role, $list_role)) {
        return $list_role[$role];
    }
}

function show_report($report) {
    $list_report = array(
        '0'=> 'Không thuộc những nội dung cơ bản',
        '1' => 'Nội dung bài viết chứa Tiếng Việt không dấu',
        '2' => 'Nội dung bài viết vi phạm nội quy của diễn đàn',
        '3' => 'Spam/Quảng cáo trong nội dung bài viết',
    );
    if (array_key_exists($report, $list_report)) {
        return $list_report[$report];
    }
}