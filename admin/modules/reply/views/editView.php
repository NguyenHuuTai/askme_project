<?php
get_header();
?>

<div id="main-content-wp" class="add-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Cập nhật câu hỏi</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <form method="POST">
                        <label for="confirm-pass">câu trả lời</label>
                        <textarea name="content" id="confirm-pass" class="ckeditor"><?php echo $info_reply['reply_content'] ?></textarea><br><br>
                        <?php echo form_error('content') ?>
                        <label for="title">trạng thái</label>
                        <select name="status">
                            <option value="">-- Chọn trạng thái --</option>
                            <option <?php if(isset($info_reply['reply_status']) && $info_reply['reply_status']=='0') echo "selected=selected"; ?> value="0">Không được phép</option>
                            <option <?php if(isset($info_reply['reply_status']) && $info_reply['reply_status']=='1') echo "selected=selected"; ?> value="1">được phép</option>
                        </select>
                        <?php echo form_error('status') ?>
                        <button type="submit" name="btn-edit" id="btn-submit">Cập nhật</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
?>