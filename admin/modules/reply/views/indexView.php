
<?php
get_header();
?>

<div id="main-content-wp" class="list-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Danh sách câu trả lời</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <div class="table-responsive">
                        <table class="table list-table-wp">
                            <thead>
                                <tr>
                                    <td><span class="thead-text">STT</span></td>
                                    <td><span class="thead-text">Bài post</span></td>
                                    <td><span class="thead-text">Người tạo</span></td>
                                    <td><span class="thead-text">Mô tả</span></td>
                                    <td><span class="thead-text">Trạng thái</span></td>
                                    <td><span class="thead-text">Ngày tạo</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($list_reply)) {
                                    $t = $start;
                                    foreach ($list_reply as $reply) {
                                        $t++;
                                        ?>
                                        <tr>
                                            <td><span class="tbody-text"><?php echo $t; ?></h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a  title=""><?php echo $reply['title'] ?></a>
                                                </div> 
                                                <ul class="list-operation fl-right">
                                                    <li><a href="<?php echo '?mod=reply&action=edit&id=' . $reply['reply_id'] ?>" title="Sửa" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                    <li><a href="<?php echo '?mod=reply&action=delete&id=' . $reply['reply_id'] ?>" title="Xóa" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text"><?php echo $reply['username'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $reply['reply_content'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo show_status($reply['reply_status'])?></span></td>
                                            <td><span class="tbody-text"><?php echo date('d/m/Y - H:i:s', $reply['createdAt'] );?></span></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="section" id="paging-wp">
                <div class="section-detail clearfix">
                    <p id="desc" class="fl-left"></p>
                    <?php
                    echo get_pagging($num_page, $page, "?mod=reply&action=index");
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>



<?php
get_footer();
?>