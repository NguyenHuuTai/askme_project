<?php

function get_reply_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `replies` WHERE `reply_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_reply($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM  `replies` JOIN `users` ON `replies`.`user_id`=`users`.`id` JOIN `post` ON `replies`.`post_id`=`post`.`post_id` {$where} ORDER BY `replies`.`createdAt` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}


function delete_reply($id) {
    return db_delete('replies', "`reply_id`='{$id}'");
}

function edit_reply($id, $data) {
    return db_update('replies', $data, "`reply_id`='{$id}'");
}
