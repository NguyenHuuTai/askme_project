<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $num_row= db_num_rows("SELECT * FROM `replies`");
    $num_per_page = 6;
    $total_row = $num_row;
    $num_page = ceil($total_row / $num_per_page);
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $start = ($page - 1) * $num_per_page;
    $list_reply = get_list_reply($start, $num_per_page);
    //show_array($list_reply);
    $data['list_reply'] = $list_reply;
    $data['page'] = $page;
    $data['num_page'] = $num_page;
    $data['start'] = $start;
    load_view('index', $data);
}


function DeleteAction() {
    $id = $_GET['id'];
    delete_reply($id);
    redirect('?mod=reply&action=index');
}

function editAction() {
    global $error, $status, $content;
    $id = $_GET['id'];
    if (isset($_POST['btn-edit'])) {
        $error = array();
        #Kiểm tra name
        if (empty($_POST['status'])) {
            $error['status'] = 'Không được để trống trạng thái';
        } else {
            $status = $_POST['status'];
        }

        if (empty($_POST['content'])) {
            $error['content'] = 'Không được để trống câu hỏi';
        } else {
            $content = $_POST['content'];
        }
        #Kết luận
        if (empty($error)) {
            $data = array(
                'reply_content' => $content,
                'reply_status' => $status,
            );
            edit_reply($id, $data);
            //Thông báo
            redirect('?mod=reply&action=index');
        }
    }
    $info_reply = get_reply_by_id($id);
    $data['info_reply'] = $info_reply;
    load_view('edit', $data);
}
