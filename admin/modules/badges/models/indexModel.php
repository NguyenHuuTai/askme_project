<?php

function get_badges_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `badges` WHERE `badges_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_badges($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `badges` {$where} LIMIT {$start},{$num_per_page}");
    return $result;
}

function add_badges($data) {
    return db_insert('badges', $data);
}

function delete_badges($id) {
    return db_delete('badges', "`badges_id`='{$id}'");
}

function edit_badges($id, $data) {
    return db_update('badges', $data, "`badges_id`='{$id}'");
}
