<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $num_row= db_num_rows("SELECT * FROM `badges`");
    $num_per_page = 6;
    $total_row = $num_row;
    $num_page = ceil($total_row / $num_per_page);
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $start = ($page - 1) * $num_per_page;
    $list_badges = get_list_badges($start, $num_per_page);
    $data['list_badges'] = $list_badges;
    $data['page'] = $page;
    $data['num_page'] = $num_page;
    $data['start'] = $start;
    load_view('index', $data);
}


function addAction() {
    global $error, $name, $description, $upload_file;
    if (isset($_POST['btn-add'])) {
        $error = array();
        #Kiểm tra name
        if (empty($_POST['name'])) {
            $error['name'] = 'Không được để trống họ và tên';
        } else {
            $name = $_POST['name'];
        }

        if (empty($_POST['description'])) {
            $error['decription'] = 'Không được để trống mô tả';
        } else {
            $description = $_POST['description'];
        }
        #Kết luận
        if (empty($error)) {
            $data1 = array(
                'name' => $name,
                'badges_description' => $description,
            );
            add_badges($data1);
            //Thông báo
            redirect('?mod=badges&action=index');
        }
    }
    load_view('add');
}

function deleteAction() {
    $id = $_GET['id'];
    delete_badges($id);
    redirect('?mod=badges&action=index');
}

function editAction() {
    global $error, $name, $description;
    $id = $_GET['id'];
    if (isset($_POST['btn-edit'])) {
        $error = array();
        #Kiểm tra name
        if (empty($_POST['name'])) {
            $error['name'] = 'Không được để trống họ và tên';
        } else {
            $name = $_POST['name'];
        }

        if (empty($_POST['description'])) {
            $error['decription'] = 'Không được để trống họ và tên';
        } else {
            $description = $_POST['description'];
        }
        #Kết luận
        if (empty($error)) {
            $data = array(
                'name' => $name,
                'badges_description' => $description,
            );
            edit_badges($id, $data);
            //Thông báo
            redirect('?mod=category&action=index');
        }
    }
    $info_badges = get_badges_by_id($id);
    $data['info_badges'] = $info_badges;
    load_view('edit', $data);
}
