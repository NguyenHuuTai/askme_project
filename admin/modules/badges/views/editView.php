<?php
get_header();
?>

<div id="main-content-wp" class="add-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Cập nhật danh hiệu</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <form method="POST">
                        <label for="old-pass">Tên danh hiệu</label>
                        <input type="text" name="name" name="pass-old" id="pass-old" value="<?php echo $info_badges['name']?>"><br><br>
                        <label for="confirm-pass">Mô Tả</label>
                        <input type="text" name="description" id="confirm-pass" value="<?php echo $info_badges['badges_description']?>"><br><br>
                        <button type="submit" name="btn-edit" id="btn-submit">Cập nhật</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php
get_footer();
?>