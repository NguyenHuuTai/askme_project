
<?php
get_header();
?>

<div id="main-content-wp" class="list-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Danh sách báo cáo câu trả lời</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <div class="table-responsive">
                        <table class="table list-table-wp">
                            <thead>
                                <tr>
                                    <td><span class="thead-text">STT</span></td>
                                    <td><span class="thead-text">ID reply</span></td>
                                    <td><span class="thead-text">Người tạo</span></td>
                                    <td><span class="thead-text">Lỗi có sẳn</span></td>
                                    <td><span class="thead-text">Lỗi ngoài lề</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($list_report_reply)) {
                                    $t = $start;
                                    foreach ($list_report_reply as $report_reply) {
                                        $t++;
                                        ?>
                                        <tr>
                                            <td><span class="tbody-text"><?php echo $t; ?></h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a  title=""><?php echo $report_reply['report_reply_id'] ?></a>
                                                </div> 
                                                <ul class="list-operation fl-right">
                                                    <li><a href="<?php echo '?mod=report_reply&action=delete&id=' . $report_reply['report_reply_id'] ?>" title="Xóa" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text"><?php echo $report_reply['username'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo show_report($report_reply['rp_content'])?></span></td>
                                            <td><span class="tbody-text"><?php echo $report_reply['rp_external'] ?></span></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
            <div class="section" id="paging-wp">
                <div class="section-detail clearfix">
                    <p id="desc" class="fl-left"></p>
                    <?php
                    echo get_pagging($num_page, $page, "?mod=report_reply&action=index");
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>



<?php
get_footer();
?>