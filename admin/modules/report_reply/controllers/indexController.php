<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $num_row= db_num_rows("SELECT * FROM `report_reply`");
    $num_per_page = 6;
    $total_row = $num_row;
    $num_page = ceil($total_row / $num_per_page);
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $start = ($page - 1) * $num_per_page;
    $list_report_reply = get_list_report_reply($start, $num_per_page);
    //show_array($list_reply);
    $data['list_report_reply'] = $list_report_reply;
    $data['page'] = $page;
    $data['num_page'] = $num_page;
    $data['start'] = $start;
    load_view('index', $data);
}


function deleteAction() {
    $id = $_GET['id'];
    delete_report_reply($id);
    redirect('?mod=report_reply&action=index');
}

