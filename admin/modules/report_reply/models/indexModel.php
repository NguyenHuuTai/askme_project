<?php

function get_reply_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `replies` WHERE `reply_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_report_reply($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM  `report_reply` JOIN `users` ON `report_reply`.`user_id`=`users`.`id` JOIN `replies` ON `report_reply`.`reply_id`=`replies`.`reply_id` {$where} ORDER BY `report_reply`.`report_reply_id` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}


function delete_report_reply($id) {
    return db_delete('report_reply', "`report_reply_id`='{$id}'");
}


