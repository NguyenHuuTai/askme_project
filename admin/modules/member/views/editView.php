<?php get_header() ?>


<div id="main-content-wp" class="info-account-page">
    <div class="section" id="title-page">
        <div class="clearfix">
            <h3 id="index" class="fl-left">Cập nhật tài khoản</h3>
        </div>
    </div>
    <div class="wrap clearfix">
        <?php 
        get_sidebar();
        ?>
        <div id="content" class="fl-right">                       
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <form action="" method="POST">
                        <label for="fullname">Tên hiển thị</label>
                        <input type="text" name="fullname" id="username" value="<?php echo $info_user['fullname']?>">
                        <?php echo form_error('fullname') ?>
                        <label for="username">Tên đăng nhập</label>
                        <input type="text" name="username" id="username" placeholder="admin" value= "<?php echo $info_user['username']?>">
                        <?php echo form_error('username') ?>
                        <label for="email">Email</label>
                        <input type="email" name="email" id="username" value="<?php echo $info_user['email']?>">
                        <?php echo form_error('email') ?>
                        <label for="tel">Số điện thoại</label>
                        <input type="tel" name="tel" id="username" value="<?php echo $info_user['phone_number']?>">
                        <?php echo form_error('tel') ?>
                        <label for="address">Địa chỉ</label>
                        <textarea name="address" id="username"><?php echo $info_user['address']?></textarea>
                        <?php echo form_error('address') ?>
                        <label for="title">Quyền</label>
                        <select name="role">
                            <option value="">-- Chọn quyền --</option>
                            <option <?php if(isset($info_user['role']) && $info_user['role']=='2') echo "selected=selected"; ?> value="2">Thành viên</option>
                            <option <?php if(isset($info_user['role']) && $info_user['role']=='1') echo "selected=selected"; ?> value="1">Admin</option>
                        </select>
                        <?php echo form_error('role') ?>  
                        <label for="title">Danh hiệu</label>
                        <select name="badges_id">
                            <option value="">-- Chọn danh hiệu --</option>
                            <option <?php if(isset($info_user['badges_id']) && $info_user['badges_id']=='2') echo "selected=selected"; ?> value="2">Thành viên</option>
                            <option <?php if(isset($info_user['badges_id']) && $info_user['badges_id']=='1') echo "selected=selected"; ?> value="1">fan cứng</option>
                        </select>
                        <?php echo form_error('badges_id') ?> 
                        <label for="title"></label>
                        <label for="title"></label>
                        <button type="submit" name="btn-update" id="btn-submit">Cập nhật</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>