
<?php
get_header();
?>

<div id="main-content-wp" class="list-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Danh sách thành viên</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <div class="table-responsive">
                        <table class="table list-table-wp">
                            <thead>
                                <tr>
                                    <td><span class="thead-text">STT</span></td>
                                    <td><span class="thead-text">Username</span></td>
                                    <td><span class="thead-text">Họ và tên</span></td>
                                    <td><span class="thead-text">Email</span></td>
                                    <td><span class="thead-text">Danh hiệu</span></td>
                                    <td><span class="thead-text">Số điện thoại</span></td>
                                    <td><span class="thead-text">Địa chỉ</span></td>
                                    <td><span class="thead-text">Quyền</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($list_users)) {
                                    $t = $start;
                                    foreach ($list_users as $user) {
                                        $t++;
                                        ?>
                                        <tr>
                                            <td><span class="tbody-text"><?php echo $t; ?></h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a  title=""><?php echo $user['username'] ?></a>
                                                </div> 
                                                <ul class="list-operation fl-right">
                                                    <li><a href="<?php echo '?mod=member&action=edit&id=' . $user['id'] ?>" title="Sửa" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                    <li><a href="<?php echo '?mod=member&action=delete&id=' . $user['id'] ?>" title="Xóa" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text"><?php echo $user['fullname'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $user['email'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $user['name'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $user['phone_number']?></span></td>
                                            <td><span class="tbody-text"><?php echo $user['address'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo show_role($user['role'])?></span></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                     
                        </table>
                    </div>
                </div>
            </div>
            <div class="section" id="paging-wp">
                <div class="section-detail clearfix">
                    <p id="desc" class="fl-left"></p>
                    <?php
                    echo get_pagging($num_page, $page, "?mod=member&action=index");
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>



<?php
get_footer();
?>