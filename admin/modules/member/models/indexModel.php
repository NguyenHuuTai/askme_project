<?php

function get_user_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `users` JOIN `badges` ON `badges`.`badges_id`=`users`.`badges_id` WHERE `id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_user($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM  `users` JOIN `badges` ON `badges`.`badges_id`=`users`.`badges_id` {$where} ORDER BY `users`.`id` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}


function delete_user($id) {
    return db_delete('user', "`id`='{$id}'");
}

function edit_user($id, $data) {
    return db_update('users', $data, "`id`='{$id}'");
}

function check_user($user1, $user2){
    if($user1==$user2)
        return true;
    return false;
}