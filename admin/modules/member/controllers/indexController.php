<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $num_row = db_num_rows("SELECT * FROM `users`");
    $num_per_page = 6;
    $total_row = $num_row;
    $num_page = ceil($total_row / $num_per_page);
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $start = ($page - 1) * $num_per_page;
    $list_users = get_list_user($start, $num_per_page);
    //show_array($list_reply);
    $data['list_users'] = $list_users;
    $data['page'] = $page;
    $data['num_page'] = $num_page;
    $data['start'] = $start;
    load_view('index', $data);
}

function DeleteAction() {
    $id = $_GET['id'];
    $user = get_user_by_id($id);
    if (!check_user($user['username'], user_login())) {
        //Kiểm tra không đươc xoa nếu ban user bạn đăng nhâp là user này
        delete_user($id);
        redirect('?mod=member&action=index');
    }
    redirect('?mod=member&action=index');
}

function editAction() {
    $id = $_GET['id'];
    global $error, $username, $phone_number, $email, $fullname, $address , $role;
    if (isset($_POST['btn-update'])) {
        $error = array();
        #Kiểm tra fullname
        if (empty($_POST['fullname'])) {
            $error['fullname'] = 'Không được để trống họ và tên';
        } else {
            $fullname = $_POST['fullname'];
        }
        #Kiểm tra sđt
        if (empty($_POST['tel'])) {
            $error['tel'] = 'Không được để trống SĐT';
        } else {
            if (strlen($_POST['tel']) > 10) {
                $error['tel'] = 'Số điện thoại không được vượt quá 12 kí tự';
            } else {
                $phone_number = $_POST['tel'];
            }
        }
        #Kiểm tra địa chỉ
        if (empty($_POST['address'])) {
            $error['address'] = 'Không được để địa chỉ';
        } else {
            $address = $_POST['address'];
        }
        #Kiểm tra email
        if (empty($_POST['email'])) {
            $error['email'] = 'Không được để email';
        } else {
            if (!is_email($_POST['email'])) {
                $error['email'] = "Tên email không đúng định dạng";
            } else {
                $email = $_POST['email'];
            }
        }
        #Kiểm tra username
        if (empty($_POST['username'])) {
            $error['username'] = 'Không được để trống tên đăng nhập';
        } else {
            if (!is_username($_POST['username'])) {
                $error['username'] = "Tên đăng nhập không đúng định dạng";
            } else {
                $username = $_POST['username'];
            }
        }
        #kiểm tra role
        if (empty($_POST['role'])) {
            $error['role'] = 'Không được để trống role';
        } else {
            $role = $_POST['role'];
        }
        #kiểm tra badges
        if (empty($_POST['badges_id'])) {
            $error['badges_id'] = 'Không được để trống role';
        } else {
            $badges_id = $_POST['badges_id'];
        }
        #Kết luận
        if (empty($error)) {
            $data = array(
                'fullname' => $fullname,
                'username' => $username,
                'phone_number' => $phone_number,
                'email' => $email,
                'address' => $address,
                'role' => $role,
                'badges_id' => $badges_id
            );
            edit_user($id, $data);
            redirect("?mod=member");
        }
    }
    $info_user = get_user_by_id($id);
    //show_array($info_user);
    $data['info_user'] = $info_user;
    load_view('edit', $data);
}
