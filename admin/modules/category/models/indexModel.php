<?php

function get_categories_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `categories` WHERE `cat_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_categories($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `categories` {$where} LIMIT {$start},{$num_per_page}");
    return $result;
}

function add_categories($data) {
    return db_insert('categories', $data);
}

function delete_categories($id) {
    return db_delete('categories', "`cat_id`='{$id}'");
}

function edit_categories($id, $data) {
    return db_update('categories', $data, "`cat_id`='{$id}'");
}
