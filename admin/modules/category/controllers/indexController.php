<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $num_row= db_num_rows("SELECT * FROM `categories`");
    $num_per_page = 10;
    $total_row = $num_row;
    $num_page = ceil($total_row / $num_per_page);
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $start = ($page - 1) * $num_per_page;
    $list_categories = get_list_categories($start, $num_per_page);
    $data['list_categories'] = $list_categories;
    $data['page'] = $page;
    $data['num_page'] = $num_page;
    $data['start'] = $start;
    load_view('index', $data);
}


function addAction() {
    global $error, $name, $description, $upload_file;
    if (isset($_POST['btn-add'])) {
        $error = array();
        #Kiểm tra name
        if (empty($_POST['name'])) {
            $error['name'] = 'Không được để trống họ và tên';
        } else {
            $name = $_POST['name'];
        }

        if (empty($_POST['description'])) {
            $error['decription'] = 'Không được để trống mô tả';
        } else {
            $description = $_POST['description'];
        }
        #Kết luận
        if (empty($error)) {
            $data1 = array(
                'name' => $name,
                'description' => $description,
            );
            add_categories($data1);
            //Thông báo
            redirect('?mod=category&action=index');
        }
    }
    load_view('add');
}

function deleteAction() {
    $id = $_GET['id'];
    delete_categories($id);
    redirect('?mod=category&action=index');
}

function editAction() {
    global $error, $name, $description;
    $id = $_GET['id'];
    if (isset($_POST['btn-edit'])) {
        $error = array();
        #Kiểm tra name
        if (empty($_POST['name'])) {
            $error['name'] = 'Không được để trống họ và tên';
        } else {
            $name = $_POST['name'];
        }

        if (empty($_POST['description'])) {
            $error['decription'] = 'Không được để trống họ và tên';
        } else {
            $description = $_POST['description'];
        }
        #Kết luận
        if (empty($error)) {
            $data = array(
                'name' => $name,
                'description' => $description,
            );
            edit_categories($id, $data);
            //Thông báo
            redirect('?mod=category&action=index');
        }
    }
    $info_categories = get_categories_by_id($id);
    $data['info_categories'] = $info_categories;
    load_view('edit', $data);
}
