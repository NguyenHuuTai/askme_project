<?php

function get_reply_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `replies` WHERE `reply_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_report_post($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM  `report_post` JOIN `users` ON `report_post`.`user_id`=`users`.`id` JOIN `post` ON `report_post`.`post_id`=`post`.`post_id` {$where} ORDER BY `report_post`.`report_post_id` DESC LIMIT {$start},{$num_per_page}");
    return $result;
}


function delete_report_post($id) {
    return db_delete('report_post', "`report_post_id`='{$id}'");
}


