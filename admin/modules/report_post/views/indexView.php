
<?php
get_header();
?>

<div id="main-content-wp" class="list-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Danh sách báo cáo câu hỏi</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <div class="table-responsive">
                        <table class="table list-table-wp">
                            <thead>
                                <tr>
                                    <td><span class="thead-text">STT</span></td>
                                    <td><span class="thead-text">Bài post</span></td>
                                    <td><span class="thead-text">Người tạo</span></td>
                                    <td><span class="thead-text">Lỗi có sẳn</span></td>
                                    <td><span class="thead-text">Lỗi ngoài lề</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($list_post_report)) {
                                    $t = $start;
                                    foreach ($list_post_report as $post_report) {
                                        $t++;
                                        ?>
                                        <tr>
                                            <td><span class="tbody-text"><?php echo $t; ?></h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a  title=""><?php echo $post_report['title'] ?></a>
                                                </div> 
                                                <ul class="list-operation fl-right">
                                                    <li><a href="<?php echo '?mod=report_post&action=delete&id=' . $post_report['report_post_id'] ?>" title="Xóa" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text"><?php echo $post_report['username'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo show_report($post_report['report_content'])  ?></span></td>
                                            <td><span class="tbody-text"><?php echo $post_report['report_external'] ?></span></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
            <div class="section" id="paging-wp">
                <div class="section-detail clearfix">
                    <p id="desc" class="fl-left"></p>
                    <?php
                    echo get_pagging($num_page, $page, "?mod=report_post&action=index");
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>



<?php
get_footer();
?>