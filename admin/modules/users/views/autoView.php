<?php get_header() ?>

<div id="main-content-wp" class="change-pass-page">
    <div class="section" id="title-page">
        <div class="clearfix">
            <h3 id="index" class="fl-left">Các chế độ tự chọn</h3>
        </div>
    </div>
    <div class="wrap clearfix">
        <?php 
        get_sidebar('users');
        ?>
        <div id="content" class="fl-right">                       
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <form method="POST">
                        <label for="title" style="font-size: 16px">Thông báo gửi về email</label><br>
                        <select id="select_email" name="select_email">
                            <option <?php if(!isset($_SESSION['auto_email'])) echo "selected=selected"; ?> value="1">Không được phép</option>
                            <option <?php if(isset($_SESSION['auto_email'])) echo "selected=selected"; ?> value="2">Được phép</option>
                        </select><br><br>
                        <label for="title" style="font-size: 16px">Duyệt tự động</label><br>
                        <select id="select_confirm" name="select_confirm">
                                <option <?php //if(!isset($_SESSION['auto_email'])) echo "selected=selected"; ?> value="1">Không được phép</option>
                            <option <?php //if(isset($_SESSION['auto_email'])) echo "selected=selected"; ?> value="2">Được phép</option>
                        </select>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php get_footer() ?>