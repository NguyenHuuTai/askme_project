
<?php

function construct() {
//    echo "DÙng chung, load đầu tiên";
    load_model('index');
    load('lib', 'validation');
    load('lib', 'email');
//    load_view('login');
}

function regLoginAction() {
    global $error, $username, $password, $username1, $password1, $email, $badges_id;
    if (isset($_POST['btn-reg'])) {
        $badges_id = 2;
        $error = array();
        #Kiểm tra email
        if (empty($_POST['email'])) {
            $error['email'] = 'Không được để email';
        } else {
            if (!is_email($_POST['email'])) {
                $error['email'] = "Tên email không đúng định dạng ";
            } else {
                $email = $_POST['email'];
            }
        }
        #Kiểm tra username
        if (empty($_POST['username'])) {
            $error['username'] = 'Không được để trống tên đăng nhập ';
        } else {
            if (!is_username($_POST['username'])) {
                $error['username'] = "Tên đăng nhập không đúng định dạng ";
            } else {
                $username = $_POST['username'];
            }
        }
        #Kiểm tra passwword
        if (empty($_POST['password'])) {
            $error['password'] = 'Không được để trống mật khẩu ';
        } else {
            if (!is_password($_POST['password'])) {
                $error['password'] = "Mật khẩu không đúng định dạng ";
            } else {
                $password = md5($_POST['password']);
            }
        }

        if (empty($_POST['confirm_password'])) {
            $error['confirm_password'] = 'Không được để trống mật khẩu xác nhận';
        } else {
            if (!is_password($_POST['confirm_password'])) {
                $error['confirm_password'] = "Mật khẩu không đúng định dạng ";
            }
            if (!compare_password($_POST['confirm_password'], $_POST['password'])) {
                $error['confirm_password'] = "Mật khẩu không giống nhau ";
            }
        }

        #Kết luận
        if (empty($error)) {
            if (!user_exists($email, $username)) {
                $data = array(
                    'username' => $username,
                    'password' => $password,
                    'email' => $email,
                    'badges_id' => $badges_id
                );
                add_user($data);
                //Thông báo
                redirect('?mod=users&action=reg');
            } else {
                $error['account'] = "Email hoặc Username đã tồn tại ";
            }
        }
    }
    if (isset($_POST['btn-login'])) {
        $error = array();
        #Kiểm tra username
        if (empty($_POST['username'])) {
            $error['username1'] = 'Không được để trống tên đăng nhập ';
        } else {
            if (!is_username($_POST['username'])) {
                $error['username1'] = "Tên đăng nhập không đúng định dạng ";
            } else {
                $username1 = $_POST['username'];
            }
        }
        #Kiểm tra passwword
        if (empty($_POST['password'])) {
            $error['password1'] = 'Không được để trống mật khẩu ';
        } else {
            if (!is_password($_POST['password'])) {
                $error['password1'] = "Mật khẩu không đúng định dạng ";
            } else {
                $password1 = md5($_POST['password']);
            }
        }
        #kết luận
        if (empty($error)) {
            if (check_login($username1, $password1)) {
                //lấy image trong user
                $image=get_user_by_username($username1);
                //lưu trữ phiên đăng nhập
                $_SESSION['is_login'] = true;
                $_SESSION['user_login'] = $username1;
                $_SESSION['admin'] = "1";
                $_SESSION['image'] = $image['avatar'];
                //Chuyển hướng vào bên trong hệ thống
                redirect();
            } else {
                $error['account1'] = 'Tên đăng nhập hoặc mật khẩu không tồn tại hoặc bạn không phải là admin ';
            }
        }
    }
    load_view('regLogin');
}

function logoutAction() {
    unset($_SESSION['is_login']);
    unset($_SESSION['user_login']);
    unset($_SESSION['admin']);
    unset($_SESSION['image']);
    redirect('?mod=users&action=login');
}

function resetAction() {
    load_view('reset');
}

function updateAction() {
    global $error, $username, $phone_number, $email, $fullname, $address;
    if (isset($_POST['btn-update'])) {
        $error = array();
        #Kiểm tra fullname
        if (empty($_POST['fullname'])) {
            $error['fullname'] = 'Không được để trống họ và tên';
        } else {
            $fullname = $_POST['fullname'];
        }
        #Kiểm tra sđt
        if (empty($_POST['tel'])) {
            $error['tel'] = 'Không được để trống SĐT';
        } else {
            if (strlen($_POST['tel']) > 12) {
                $error['tel'] = 'Số điện thoại không được vượt quá 12 kí tự';
            } else {
                $phone_number = $_POST['tel'];
            }
        }
        #Kiểm tra địa chỉ
        if (empty($_POST['address'])) {
            $error['address'] = 'Không được để địa chỉ';
        } else {
            $address = $_POST['address'];
        }
        #Kiểm tra email
        if (empty($_POST['email'])) {
            $error['email'] = 'Không được để email';
        } else {
            if (!is_email($_POST['email'])) {
                $error['email'] = "Tên email không đúng định dạng";
            } else {
                $email = $_POST['email'];
            }
        }
        #Kiểm tra username
        if (empty($_POST['username'])) {
            $error['username'] = 'Không được để trống tên đăng nhập';
        } else {
            if (!is_username($_POST['username'])) {
                $error['username'] = "Tên đăng nhập không đúng định dạng";
            } else {
                $username = $_POST['username'];
            }
        }

        #Kết luận
        if (empty($error)) {
            $data = array(
                'fullname' => $fullname,
                'username' => $username,
                'phone_number' => $phone_number,
                'email' => $email,
                'address' => $address
            );
            update_user_login(user_login(), $data);
        }
    }
    $info_user = get_user_by_username(user_login());
    //show_array($info_user);
    $data['info_user'] = $info_user;
    load_view('update', $data);
}

function autoAction() {
    load_view('auto');
}

function auto_emailAction() {
    $option = $_POST['option'];
    $info_user = get_user_by_username(user_login());
    if ($option == '1') {
        if (isset($_SESSION['auto_email'])) {
            unset($_SESSION['auto_email']);
            echo "Bạn đã tắt thông báo về email khi có bài post mới";
        } else {
            echo "Bạn đã tắt thông báo về email khi có bài post mới";
        }
    } else {
        $_SESSION['auto_email'] = $info_user['email'];
        echo "Bạn đã bật thông báo về email khi có bài post mới";
    }
}
