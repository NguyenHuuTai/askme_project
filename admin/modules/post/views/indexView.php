<?php
get_header();
?>

<div id="main-content-wp" class="list-cat-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Danh sách câu hỏi</h3>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <div class="table-responsive">
                        <table class="table list-table-wp">
                            <thead>
                                <tr>
                                    <td><span class="thead-text">STT</span></td>
                                    <td><span class="thead-text">Tiêu đề</span></td>
                                    <td><span class="thead-text">Câu hỏi</span></td>
                                    <td><span class="thead-text">Trạng thái</span></td>
                                    <td><span class="thead-text">Danh mục</span></td>
                                    <td><span class="thead-text">Tag</span></td>
                                    <td><span class="thead-text">Người tạo</span></td>
                                    <td><span class="thead-text">Đánh giá</span></td>
                                    <td><span class="thead-text">Tổng like</span></td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($list_post)) {
                                    $t = $start;
                                    foreach ($list_post as $post) {
                                        $t++;
                                        ?>
                                        <tr>   
                                            <td><span class="tbody-text"><?php echo $t; ?></h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a href="" title=""><?php echo $post['title'] ?></a>
                                                </div> 
                                                <ul class="list-operation fl-right">
                                                    <li><a href="<?php echo '?mod=post&action=edit&id=' . $post['post_id'] ?>" title="Sửa" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                    <li><a href="<?php echo '?mod=post&action=delete&id=' . $post['post_id'] ?>" title="Xóa" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text"><?php echo $post['content'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo show_status($post['status'])?></span></td>
                                            <td><span class="tbody-text"><?php echo $post['name'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $post['tag_name'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $post['username'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $post['total_star'] ?></span></td>
                                            <td><span class="tbody-text"><?php echo $post['total_like'] ?></span></td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            
                        </table>
                    </div>
                </div>
            </div>
            <div class="section" id="paging-wp">
                <div class="section-detail clearfix">
                    <p id="desc" class="fl-left">Chọn vào checkbox để lựa chọn tất cả</p>
                    <?php
                    echo get_pagging($num_page, $page, "?mod=post&action=index");
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>

<?php
get_footer();
?>