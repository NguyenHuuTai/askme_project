<?php

function construct() {
    load_model('index');
    load('lib', 'validation');
    load('lib', 'pagging');
}

function indexAction() {
    $num_row = db_num_rows("SELECT * FROM `post`");
    $num_per_page = 6;
    $total_row = $num_row;
    $num_page = ceil($total_row / $num_per_page);
    $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
    $start = ($page - 1) * $num_per_page;
    $list_post = get_list_post($start, $num_per_page);
    $data['list_post'] = $list_post;
    $data['page'] = $page;
    $data['num_page'] = $num_page;
    $data['start'] = $start;
    load_view('index', $data);
}

function deleteAction() {
    $id = $_GET['id'];
    if (check_status($id)) {
        delete_post($id);
        redirect('?mod=post&action=index');
    }
    redirect('?mod=post&action=index');
}

function editAction() {
    global $error, $status, $content;
    $id = $_GET['id'];
    if (isset($_POST['btn-edit'])) {
        $error = array();
        #Kiểm tra name
        if (empty($_POST['status'])) {
            $error['status'] = 'Không được để trống trạng thái';
        } else {
            $status = $_POST['status'];
        }

        if (empty($_POST['content'])) {
            $error['content'] = 'Không được để trống câu hỏi';
        } else {
            $content = $_POST['content'];
        }
        #Kết luận
        if (empty($error)) {
            $data = array(
                'content' => $content,
                'status' => $status,
            );
            edit_post($id, $data);
            //Thông báo
            redirect('?mod=post&action=index');
        }
    }
    $info_post = get_post_by_id($id);
    $data['info_post'] = $info_post;
    load_view('edit', $data);
}
