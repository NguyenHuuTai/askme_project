<?php

function get_list_post($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `post` JOIN `users` ON `post`.`author_id`=`users`.`id`JOIN `categories` ON `post`.`cat_id`=`categories`.`cat_id` JOIN `tag` ON `tag`.`tag_id`= `post`.`tag_id` ORDER BY `post`.`post_id` DESC {$where} LIMIT {$start},{$num_per_page}");
    return $result;
}

function get_post_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `post` WHERE `post_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function edit_post($id, $data) {
    return db_update('post', $data, "`post_id`='{$id}'");
}

function delete_post($id) {
    return db_delete('post', "`post_id`='{$id}'");
}

function check_status($id) {
    $item = db_fetch_row("SELECT status FROM `post` WHERE `post_id` = '{$id}'");
    if ($item['status']=='0')
        return true;
    return false;
}
