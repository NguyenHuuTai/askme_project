
<?php
get_header();
?>

<div id="main-content-wp" class="list-tag-page">
    <div class="wrap clearfix">
        <?php require 'layout/sidebar.php'; ?>
        <div id="content" class="fl-right">
            <div class="section" id="title-page">
                <div class="clearfix">
                    <h3 id="index" class="fl-left">Danh sách tag</h3>
                    <a href="?mod=tag&action=add" title="" id="add-new" class="fl-left">Thêm mới</a>
                </div>
            </div>
            <div class="section" id="detail-page">
                <div class="section-detail">
                    <div class="table-responsive">
                        <table class="table list-table-wp">
                            <thead>
                                <tr>
                                    
                                    <td><span class="thead-text">STT</span></td>
                                    <td><span class="thead-text">Tiêu đề</span></td>
                                    <td><span class="thead-text">Mô tả</span></td>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($list_tag)) {
                                    $t = $start;
                                    foreach ($list_tag as $tag) {
                                        $t++;
                                        ?>
                                        <tr>
                                            
                                            <td><span class="tbody-text"><?php echo $t; ?></h3></span>
                                            <td class="clearfix">
                                                <div class="tb-title fl-left">
                                                    <a  title=""><?php echo $tag['tag_name'] ?></a>
                                                </div> 
                                                <ul class="list-operation fl-right">
                                                    <li><a href="<?php echo '?mod=tag&action=edit&id=' . $tag['tag_id'] ?>" title="Sửa" class="edit"><i class="fa fa-pencil" aria-hidden="true"></i></a></li>
                                                    <li><a href="<?php echo '?mod=tag&action=delete&id=' . $tag['tag_id'] ?>" title="Xóa" class="delete"><i class="fa fa-trash" aria-hidden="true"></i></a></li>
                                                </ul>
                                            </td>
                                            <td><span class="tbody-text"><?php echo $tag['description'] ?></span></td>
                                           
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    
                                    <td><span class="tfoot-text">STT</span></td>
                                    <td><span class="tfoot-text-text">Tiêu đề</span></td>
                                    <td><span class="tfoot-text">Mô tả</span></td>
                                           
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
            <div class="section" id="paging-wp">
                <div class="section-detail clearfix">
                    
                    <?php
                    echo get_pagging($num_page, $page, "?mod=tag&action=index");
                    ?>
                </div>

            </div>
        </div>
    </div>
</div>



<?php
get_footer();
?>