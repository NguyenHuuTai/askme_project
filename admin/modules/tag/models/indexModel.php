<?php

function get_tag_by_id($id) {
    $item = db_fetch_row("SELECT * FROM `tag` WHERE `tag_id` = '{$id}'");
    if (!empty($item))
        return $item;
}

function get_list_tag($start, $num_per_page = 10, $where = "") {
    if (!empty($where)) {
        $where = "WHERE {$where}";
    }
    $result = db_fetch_array("SELECT * FROM `tag` {$where} LIMIT {$start},{$num_per_page}");
    return $result;
}

function add_tag($data) {
    return db_insert('tag', $data);
}

function delete_tag($id) {
    return db_delete('tag', "`tag_id`='{$id}'");
}

function edit_tag($id, $data) {
    return db_update('tag', $data, "`tag_id`='{$id}'");
}
