<div id="sidebar" class="fl-left">
    <ul id="sidebar-menu">
        <li class="nav-item">
            <a href="" title="" class="nav-link nav-toggle">
                <span class="fa fa-map icon"></span>
                <span class="title">Post</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=post" title="" class="nav-link">Danh sách các trang</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" title="" class="nav-link nav-toggle">
                <span class="fa fa-cubes icon"></span>
                <span class="title">Reply</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=reply" title="" class="nav-link">Danh sách reply</a>
                </li>
            </ul>
        </li>
         <li class="nav-item">
            <a href="" title="" class="nav-link nav-toggle">
                <span class="fa fa-database icon"></span>
                <span class="title">Member</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=member" title="" class="nav-link">Danh sách member</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="" title="" class="nav-link nav-toggle">
                <span class="fa fa-pencil-square-o icon"></span>
                <span class="title">Category</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=category&action=add" title="" class="nav-link">Thêm mới</a>
                </li>
                <li class="nav-item">
                    <a href="?mod=category" title="" class="nav-link">Danh sách Category</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="" title="" class="nav-link nav-toggle">
                <span class="fa fa-pencil-square-o icon"></span>
                <span class="title">Tag</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=tag&action=add" title="" class="nav-link">Thêm mới</a>
                </li>
                <li class="nav-item">
                    <a href="?mod=tag" title="" class="nav-link">Danh sách Tag</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" title="" class="nav-link nav-toggle">
                <i class="fa fa-pencil-square-o icon" aria-hidden="true"></i>
                <span class="title">Danh hiệu</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=badges" title="" class="nav-link">Danh sách danh hiệu</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" title="" class="nav-link nav-toggle">
                <i class="fa fa-sliders" aria-hidden="true"></i>
                <span class="title">Report post</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=report_post" title="" class="nav-link">Danh sách Report post</a>
                </li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="#" title="" class="nav-link nav-toggle">
                <i class="fa fa-file-image-o" aria-hidden="true"></i>
                <span class="title">Report reply</span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item">
                    <a href="?mod=report_reply" title="" class="nav-link">Danh sách Report reply</a>
                </li>
            </ul>
        </li>
        
    </ul>
</div>
