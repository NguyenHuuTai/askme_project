<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ask?</title>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css"
              integrity="sha256-46r060N2LrChLLb5zowXQ72/iKKNiw/lAmygmHExk/o=" crossorigin="anonymous" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
              integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link href="public/css/home.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/create-post.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/post_detail.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/profile.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/update_password.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/update_profile.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/history.css" rel="stylesheet" type="text/css"/>
        <link href="public/css/top_user.css" rel="stylesheet" type="text/css"/>
        <script src="public/js/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    </head>

    <body>
        <header class="header">
            <div class="header-logo">
                <a href="?mod=home">
                    <img src="public/images/logo.png" class="header-logo__img">
                </a>
            </div>
            <form action="?mod=home&controller=index&action=search" method="POST">
                <div class="header-search">
                    <input class="header-search__input" placeholder="Search all forums" name="title">
                    <div class="header-search__button">
                        <i class="fas fa-search"><button type="submit" name="btn-search">search</button></i>
                    </div>
                </div>
            </form>
            <div class="header-post">
                <div class="header-post__icon">
                    <i class="far fa-plus-square"></i>
                </div>
                <div class="header-post__text">
                    <p><a href="?mod=post&action=add">Tạo bài viết mới</a></p>
                </div>
            </div>
            <div class="header-notification">
                <div class="header-notification__button">
                    <i class="fas fa-bell"></i>
                </div>
                <div class="header-notification__dropdown"></div>
            </div>
            <div class="header-user">
                <div class="dropdown header-user__dropdown">
                    <button class="btn btn-secondary dropdown-toggle nav-dropdown-btn" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="user-info__image">
                            <img src="<?php if(isset($_SESSION['user_login'])){ echo $_SESSION['image'];}else{ echo 'public/images/user_image_default.png';}?>">
                        </div>
                        <div class="user-info__name">
                            <p><?php if(isset($_SESSION['user_login'])) echo $_SESSION['user_login']; ?></p>
                        </div>
                    </button>
                    <div class="dropdown-menu nav-dropdown-list nav-dropdown-list-user"
                         aria-labelledby="dropdownMenuButton">
                        <ul class="user-dropdown__list">
                            <li>
                                <i class="far fa-user-circle"></i>
                                <p><a href="?mod=users&controller=index&action=index">Thông tin cá nhân</a></p>
                            </li>
                            <li>
                                <i class="fas fa-history"></i>
                                <p><a href="?mod=users&controller=index&action=history">Lịch sử hoạt động</a></p>
                            </li>
                            <li>
                                <i class="fas fa-sign-out-alt"></i>
                                <p><?php
                                    if (!isset($_SESSION['user_login'])) {
                                        echo "<a href='?mod=users&action=regLogin'>Đăng nhập</a>";
                                    } else {
                                        echo "<a href='?mod=users&action=logout' >Đăng xuất<a>";
                                    }
                                    ?></p>
                            </li>
                        </ul>
                    </div>
                </div>
        </header>